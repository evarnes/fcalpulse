#!/bin/bash

runDir=/home/varnes/FCalPulse/2022/production/fcalpulse/

#kill ControlHost dispatcher
#PID=`ps -ef | grep ${runDir}/ControlHost/ControlHost/bin/dispatch | grep -v gr#ep | awk '{print $2}'`
#if [[ $PID -ne "" ]]
#then
#    kill ${PID}
#fi


#kill register communication process
PID=`ps -ef | grep ${runDir}/FCP_DAQ/src/beam_signal | grep -v grep | awk '{print $2}'`
if [[ $PID -ne "" ]]
then
    kill $PID
fi

PID=`ps -ef | grep spsSpill | grep -v grep | grep -v emacs | grep -v less | awk '{print $2}'`
if [[ $PID -ne "" ]]
then
    kill $PID
fi

