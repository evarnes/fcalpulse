#!/bin/bash

if test -f "/home/varnes/FCalPulse/conditions/FCalPulse_HV_Settings.txt"; then
    rm /home/varnes/FCalPulse/conditions/FCalPulse_HV_Settings.txt
fi

#ssh FcalPulseHV@128.141.151.225 `ls -t C:\FCalPulse_Data\HV_Record\ | head -1` > lastest_HV_file.txt &

#sleep 0.1

#at CERN in 2022
scp  -P 1533 -o "ConnectTimeout = 1" FCalPulseHV@128.141.149.224:Current_FCalPulse_HV.txt /home/varnes/FCalPulse/conditions/FCalPulse_HV_Settings.txt >& /dev/null 

#10.208.15.241
sleep 0.10
