#!/bin/bash

runDir=/home/varnes/FCalPulse/2022/production/fcalpulse/

#kill pre-existing processes if needed
${runDir}/scripts/kill_processes.sh

#start ControlHost dispatcher
#PID=`ps -ef | grep ${runDir}/ControlHost/ControlHost/bin/dispatch | grep -v gr#ep | awk '{print $2}'`
#if [[ $PID -eq "" ]]
#then
    nohup ${runDir}/ControlHost/ControlHost/bin/dispatch &
#fi

#start register communication
#${runDir}/FCP_DAQ/src/beam_signal &
#/usr/local/anaconda3/bin/python ~rutherfo/main/FCalPulse/TestBeam/spsSpill.v05.py &
/usr/local/anaconda3/bin/python ${runDir}/FCP_DAQ/src/spsSpill.py >& /dev/null  &
#/usr/local/anaconda3/bin/python spsSpill.py &
echo 'Started spsSpill.py'

#check if processes are alive
numDispatchers=`ps -ef | grep ${runDir}/ControlHost/ControlHost/bin/dispatch | grep -v grep | wc -l`
if [[ $numDispatchers -gt 0 ]]
then
    if [[ $numDispatchers -eq 1 ]]
    then
	echo "ControlHost dispatcher started OK"
    else
	echo "Multiple ControlHost dispatchers running.  Please investigate."
    fi
else
  echo "Problem starting ControlHost dispatcher.  Please investigate"
fi

numSpsSpills=`ps -ef | grep ${runDir}/FCP_DAQ/src/spsSpill.py | grep -v  grep | grep -v emacs | wc -l`
if [[ $numSpsSpills -gt 0 ]]                                             
then                                                                           
    if [[ $numSpsSpills -eq 1 ]]                                         
    then                                                                       
       echo "spsSpill.py started OK"                                
    else                                                                       
       echo "Multiple spsSpill.py processes running.  Please investigate."
    fi                                                                         
else                                                                           
  echo "Problem starting spsSpill.py process.  Please investigate"  
fi

#numRegisterSockets=`ps -ef | grep ${runDir}/FCP_DAQ/src/beam_signal | grep -v #grep | wc -l`

#if [[ $numRegisterSockets -gt 0 ]]
#then
#    if [[ $numRegisterSockets -eq 1 ]]
#    then
#	echo "Register communication started OK"
#    else
#	echo "Multiple register communication processes running.  Please investigate."
#    fi
#else
#  echo "Problem starting register communication process.  Please investigate"
#fi
