#ifndef SiteConfig_h
#define SiteConfig_h

class SiteConfig {

 public:

  //the following is for lxplus
  //static constexpr char digiDataDir[] = "/eos/atlas/atlascerngroupdisk/larg-upgrade/phase-2/FCalPulse/data/2022/all/";
  //static constexpr char BPCDataDir[] = "/eos/atlas/atlascerngroupdisk/larg-upgrade/phase-2/FCalPulse/data/2022/BPCdata/";
  //static constexpr char BPCCalibDir[] = "/eos/atlas/atlascerngroupdisk/larg-upgrade/phase-2/FCalPulse/data/2022/BPCcalib/";

  // following is for atlng (Arizona)
  static constexpr char digiDataDir[] = "/data1/tb2022/data/2022/all/";
  static constexpr char BPCDataDir[] = "/data1/savin/FCal2022/BPCdata/";
  static constexpr char BPCCalibDir[] = "/home/savin/FCalPulse2022/calibration/";

  
};



#endif
