#ifndef DIG_UTILS_H
#define DIG_UTILS_H
/* ---------------------------------------------------------------------
// Collection of routines useful in analysis digitizer data
-----------------------------------------------------------------------*/
#include "Rtypes.h"
#include <vector>
#include "TChain.h"
#include "SiteConfig.h"
#include "TH1F.h"

class Dig_utils {

 public:

  Dig_utils() {};
  
  Int_t TimingCorrection(std::vector<unsigned short> *trigger_data, int idig);
  Double_t GetPedestal(std::vector<unsigned short> *samples , unsigned nSamples);
  Double_t GetPedestal(TH1F* h , unsigned nSamples);

  TChain* GetData(Int_t HV, Bool_t Sr90in);

  Float_t GetArea(TH1F *h);

  static Double_t ExpectedPulseArea(double *x, double *par);
  
 private:

  void AddToChainInSpillOrder(TChain* t, Int_t run);

};
#endif
