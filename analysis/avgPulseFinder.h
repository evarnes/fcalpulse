#ifndef AVGPULSEFINDER_H
#define AVGPULSEFINDER_H

#include "FCPAnalysisBase.h"
#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1F.h>

class avgPulseFinder : public FCPAnalysisBase {
public :

  avgPulseFinder(Int_t HV, Bool_t Sr90in);

  virtual void      Init(TTree *t);
  virtual Int_t     Execute();
  virtual void      Finalize();
  
  TH1F*  Get_h_mid() { return h_mid; }

 private:

  TH1F *h_front, *h_mid, *h_rear;
  TH1F *h_front_trigfail, *h_mid_trigfail, *h_rear_trigfail;
  
  TH1F *h_front_scal, *h_rear_scal;
  
  TH1F *h_triggerTime_1, *h_triggerTime_2, *h_triggerTime_3, *h_triggerTime_4;

  Bool_t Sr90in;
  Int_t smin;
  Int_t smax;
  Int_t nbins;
  Int_t nPassTrig;
  Int_t idig_source;
  Int_t idig_nosource;

};
#endif
