#include "BPC_leaves.h"

int BPC_leaves::SetBranchAddress() {
  bpcD->SetBranchAddress("t", &tBPCsec);
  bpcD->SetBranchAddress("tTDCtrig", &tBPC);
  bpcD->SetBranchAddress("pattern", &pattern);

  bpcD->SetBranchAddress("dwc1left_nt00",  &x1l_n, &b_x1l_n);
  bpcD->SetBranchAddress("dwc1left_tt00",   x1l_t, &b_x1l_t);
  bpcD->SetBranchAddress("dwc1right_nt01", &x1r_n, &b_x1r_n);
  bpcD->SetBranchAddress("dwc1right_tt01",  x1r_t, &b_x1r_t);

  bpcD->SetBranchAddress("dwc1up_nt02",    &y1u_n, &b_y1u_n);
  bpcD->SetBranchAddress("dwc1up_tt02",     y1u_t, &b_y1u_t);
  bpcD->SetBranchAddress("dwc1down_nt03",  &y1d_n, &b_y1d_n);
  bpcD->SetBranchAddress("dwc1down_tt03",   y1d_t, &b_y1d_t);

  bpcD->SetBranchAddress("dwc2left_nt04",  &x2l_n, &b_x2l_n);
  bpcD->SetBranchAddress("dwc2left_tt04",   x2l_t, &b_x2l_t);
  bpcD->SetBranchAddress("dwc2right_nt05", &x2r_n, &b_x2r_n);
  bpcD->SetBranchAddress("dwc2right_tt05",  x2r_t, &b_x2r_t);

  bpcD->SetBranchAddress("dwc2up_nt06",    &y2u_n, &b_y2u_n);
  bpcD->SetBranchAddress("dwc2up_tt06",     y2u_t, &b_y2u_t);
  bpcD->SetBranchAddress("dwc2down_nt07",  &y2d_n, &b_y2d_n);
  bpcD->SetBranchAddress("dwc2down_tt07",   y2d_t, &b_y2d_t);

  bpcD->SetBranchAddress("dwc3left_nt08",  &x3l_n, &b_x3l_n);
  bpcD->SetBranchAddress("dwc3left_tt08",   x3l_t, &b_x3l_t);
  bpcD->SetBranchAddress("dwc3right_nt09", &x3r_n, &b_x3r_n);
  bpcD->SetBranchAddress("dwc3right_tt09",  x3r_t, &b_x3r_t);

  bpcD->SetBranchAddress("dwc3up_nt10",    &y3u_n, &b_y3u_n);
  bpcD->SetBranchAddress("dwc3up_tt10",     y3u_t, &b_y3u_t);
  bpcD->SetBranchAddress("dwc3down_nt11",  &y3d_n, &b_y3d_n);
  bpcD->SetBranchAddress("dwc3down_tt11",   y3d_t, &b_y3d_t);

  bpcD->SetBranchAddress("dwc4left_nt12",  &x4l_n, &b_x3l_n);
  bpcD->SetBranchAddress("dwc4left_tt12",   x4l_t, &b_x3l_t);
  bpcD->SetBranchAddress("dwc4right_nt13", &x4r_n, &b_x3r_n);
  bpcD->SetBranchAddress("dwc4right_tt13",  x4r_t, &b_x3r_t);

  bpcD->SetBranchAddress("dwc4up_nt14",    &y4u_n, &b_y4u_n);
  bpcD->SetBranchAddress("dwc4up_tt14",     y4u_t, &b_y4u_t);
  bpcD->SetBranchAddress("dwc4down_nt15",  &y4d_n, &b_y4d_n);
  bpcD->SetBranchAddress("dwc4down_tt15",   y4d_t, &b_y4d_t);

  bpcD->SetBranchAddress("x1",&x_first[0]);
  bpcD->SetBranchAddress("x2",&x_first[1]);
  bpcD->SetBranchAddress("x3",&x_first[2]);
  bpcD->SetBranchAddress("x4",&x_first[3]);
  bpcD->SetBranchAddress("y1",&y_first[0]);
  bpcD->SetBranchAddress("y2",&y_first[1]);
  bpcD->SetBranchAddress("y3",&y_first[2]);
  bpcD->SetBranchAddress("y4",&y_first[3]);

  return 0;
}
