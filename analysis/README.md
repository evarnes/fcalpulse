# fcalpulse/analysis

Analysis code for the FCalPulse test beam.

## Installation

To obtain the code, do

```bash
git clone ssh://git@gitlab.cern.ch:7999/evarnes/fcalpulse.git
```

The framework is designed to allow one to write analysis code as a derived class from FCPAnalysisBase, and then use that derived class within a main program (main programs have the .cpp extension).

Examples of this can be seen in pulseFractionWriter.[h,C], and the main program than uses it, pulseFractions.cpp.

Once you have written your .cpp file, you will need to:

* edit SiteConfig.h to point to the places where the testbeam data is stored (by default it uses the data location on lxplus).

* set up ROOT
  * on lxplus this is done by:
```bash
setupATLAS
lsetup "root 6.28.00-x86_64-centos7-gcc11-opt" 
```

* type
```bash
make
```
to compile the code.


## Contributing

Anyone can add features to the Dig_utils and BPC_utils classes, new analysis classes (derived from FCPAnalysisBase) and new main programs.  If there is a need for changes to FCPAnalysisBase, please open an issue to discuss what you would like to change.