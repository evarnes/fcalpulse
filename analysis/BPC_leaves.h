#ifndef BPC_leaves_h
#define BPC_leaves_h

#include <TTree.h>

class BPC_leaves {

 public:

  void Init(TTree* bpcTree) {bpcD = bpcTree; SetBranchAddress();}

  TTree *bpcD;
  const Int_t maxHits=100;
  int    tBPC;
  double tBPCsec;
  int    pattern;
  double x_first[4];
  double y_first[4];
  
  // declaration of leaf types
  Int_t  x1l_n;           // number of hits left
  Int_t  x1l_t[100];  // hits left
  Int_t  x1r_n;          // number of hits right
  Int_t  x1r_t[100]; // hits right
  
  Int_t  y1u_n;           // number of hits up
  Int_t  y1u_t[100];  // hits up
  Int_t  y1d_n;          // number of hits down
  Int_t  y1d_t[100]; // hits rdown
  
  Int_t  x2l_n;           // number of hits left
  Int_t  x2l_t[100];  // hits left
  Int_t  x2r_n;          // number of hits right
  Int_t  x2r_t[100]; // hits right
  
  Int_t  y2u_n;           // number of hits up
  Int_t  y2u_t[100];  // hits up
  Int_t  y2d_n;          // number of hits down
  Int_t  y2d_t[100]; // hits rdown
  
  Int_t  x3l_n;           // number of hits left
  Int_t  x3l_t[100];  // hits left
  Int_t  x3r_n;          // number of hits right
  Int_t  x3r_t[100]; // hits right
 
  Int_t  y3u_n;           // number of hits up
  Int_t  y3u_t[100];  // hits up
  Int_t  y3d_n;          // number of hits down
  Int_t  y3d_t[100]; // hits rdown
  
  Int_t  x4l_n;           // number of hits left
  Int_t  x4l_t[100];  // hits left
  Int_t  x4r_n;          // number of hits right
  Int_t  x4r_t[100]; // hits right
  
  Int_t  y4u_n;           // number of hits up
  Int_t  y4u_t[100];  // hits up
  Int_t  y4d_n;          // number of hits down
  Int_t  y4d_t[100]; // hits rdown
  
  TBranch        *b_x1l_n;   //!
  TBranch        *b_x1l_t;   //!
  TBranch        *b_x1r_n;   //!
  TBranch        *b_x1r_t;   //!
  
  TBranch        *b_y1u_n;   //!
  TBranch        *b_y1u_t;   //!
  TBranch        *b_y1d_n;   //!
  TBranch        *b_y1d_t;   //!
  
  TBranch        *b_x2l_n;   //!
  TBranch        *b_x2l_t;   //!
  TBranch        *b_x2r_n;   //!
  TBranch        *b_x2r_t;   //!
  
  TBranch        *b_y2u_n;   //!
  TBranch        *b_y2u_t;   //!
  TBranch        *b_y2d_n;   //!
  TBranch        *b_y2d_t;   //!
  
  TBranch        *b_x3l_n;   //!
  TBranch        *b_x3l_t;   //!
  TBranch        *b_x3r_n;   //!
  TBranch        *b_x3r_t;   //!
  
  TBranch        *b_y3u_n;   //!
  TBranch        *b_y3u_t;   //!
  TBranch        *b_y3d_n;   //!
  TBranch        *b_y3d_t;   //!
  
  TBranch        *b_x4l_n;   //!
  TBranch        *b_x4l_t;   //!
  TBranch        *b_x4r_n;   //!
  TBranch        *b_x4r_t;   //!
  
  TBranch        *b_y4u_n;   //!
  TBranch        *b_y4u_t;   //!
  TBranch        *b_y4d_n;   //!
  TBranch        *b_y4d_t;   //!

  int SetBranchAddress();

};
#endif
