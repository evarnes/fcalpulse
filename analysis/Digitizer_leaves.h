#ifndef DIGITIZER_LEAVES_H
#define DIGITIZER_LEAVES_H

#include <TTree.h>
#include <Rtypes.h>

class Digitizer_leaves {

 public:

  Digitizer_leaves(TTree *digTree) {Init(digTree);}
  void Init(TTree* digTree);

  TTree* digD;
  
  // declaration of leaf types
   UInt_t          dataFormatVersion;
   UInt_t          channelDataSize;
   UInt_t          RecordLength;
   UInt_t          PostTrigger;
   UInt_t          FPIOtype;
   UInt_t          NumDigitizers;
   UInt_t          SerialNumber[4];
   UInt_t          ExtTriggerMode[4];
   UInt_t          EnableMask[4];
   UInt_t          DCoffset[4][8];
   UInt_t          ChannelTriggerMode[4][8];
   UInt_t          PulsePolarity[4][8];
   UInt_t          GWn;
   UInt_t          GWaddr[20];
   UInt_t          GWdata[20];
   UInt_t          GWmask[20];
   UInt_t          nCryostats;
   UShort_t        scintUsed;
   UShort_t        includeTailVeto;
   UShort_t        sourceInBeam;
   UInt_t          runNumber;
   UInt_t          spillNumber;
   TDatime         *spillStartTime;
   UInt_t          BeamStatus;
   UInt_t          digitizerFailBits[4];
   Float_t         SCal_HV_set;
   Float_t         SCal_HV_read;
   Float_t         FCal_inner_HV_set;
   Float_t         FCal_tubeA_HV_read;
   Float_t         FCal_outer_HV_set;
   Float_t         FCal_tubeB_HV_read;
   Float_t         FCal_tubeC_HV_read;
   UInt_t          NumEventsMax;
   UInt_t          NumEventsInSpill;
   UInt_t          nchannels;
   UInt_t          eventCounter;
   UInt_t          triggerTimeTag[4];
   std::vector<unsigned short> *FCal_rod_front_source_data;
   std::vector<unsigned short> *FCal_rod_mid_source_data;
   std::vector<unsigned short> *FCal_rod_rear_source_data;
   std::vector<unsigned short> *ShCal_front_source_quad_1_data;
   std::vector<unsigned short> *ShCal_front_source_quad_2_data;
   std::vector<unsigned short> *ShCal_front_source_quad_3_data;
   std::vector<unsigned short> *ShCal_front_source_quad_4_data;
   std::vector<unsigned short> *ShCal_rear_source_quad_1_data;
   std::vector<unsigned short> *ShCal_rear_source_quad_2_data;
   std::vector<unsigned short> *ShCal_rear_source_quad_3_data;
   std::vector<unsigned short> *ShCal_rear_source_quad_4_data;
   std::vector<unsigned short> *FCal_rod_front_nosource_data;
   std::vector<unsigned short> *FCal_rod_mid_nosource_data;
   std::vector<unsigned short> *FCal_rod_rear_nosource_data;
   std::vector<unsigned short> *ShCal_front_nosource_quad_1_data;
   std::vector<unsigned short> *ShCal_front_nosource_quad_2_data;
   std::vector<unsigned short> *ShCal_front_nosource_quad_3_data;
   std::vector<unsigned short> *ShCal_front_nosource_quad_4_data;
   std::vector<unsigned short> *ShCal_rear_nosource_quad_1_data;
   std::vector<unsigned short> *ShCal_rear_nosource_quad_2_data;
   std::vector<unsigned short> *ShCal_rear_nosource_quad_3_data;
   std::vector<unsigned short> *ShCal_rear_nosource_quad_4_data;
   std::vector<unsigned short> *S1_counter_data;
   std::vector<unsigned short> *S2_counter_data;
   std::vector<unsigned short> *S3_counter_data;
   std::vector<unsigned short> *S4_counter_data;
   std::vector<unsigned short> *Leakage_counter_data;
   std::vector<unsigned short> *Halo_counter_data;
   std::vector<unsigned short> *Digitizer_1_trigger_data;
   std::vector<unsigned short> *Digitizer_2_trigger_data;
   std::vector<unsigned short> *Digitizer_3_trigger_data;
   std::vector<unsigned short> *Digitizer_4_trigger_data;
   UShort_t        FCal_rod_front_source_digitizer_SN;
   UShort_t        FCal_rod_front_source_digitizer_handle;
   UShort_t        FCal_rod_front_source_channel;
   Float_t         FCal_rod_front_source_vrange;
   UShort_t        FCal_rod_mid_source_digitizer_SN;
   UShort_t        FCal_rod_mid_source_digitizer_handle;
   UShort_t        FCal_rod_mid_source_channel;
   Float_t         FCal_rod_mid_source_vrange;
   UShort_t        FCal_rod_rear_source_digitizer_SN;
   UShort_t        FCal_rod_rear_source_digitizer_handle;
   UShort_t        FCal_rod_rear_source_channel;
   Float_t         FCal_rod_rear_source_vrange;
   UShort_t        ShCal_front_source_quad_1_digitizer_SN;
   UShort_t        ShCal_front_source_quad_1_digitizer_handle;
   UShort_t        ShCal_front_source_quad_1_channel;
   Float_t         ShCal_front_source_quad_1_vrange;
   UShort_t        ShCal_front_source_quad_2_digitizer_SN;
   UShort_t        ShCal_front_source_quad_2_digitizer_handle;
   UShort_t        ShCal_front_source_quad_2_channel;
   Float_t         ShCal_front_source_quad_2_vrange;
   UShort_t        ShCal_front_source_quad_3_digitizer_SN;
   UShort_t        ShCal_front_source_quad_3_digitizer_handle;
   UShort_t        ShCal_front_source_quad_3_channel;
   Float_t         ShCal_front_source_quad_3_vrange;
   UShort_t        ShCal_front_source_quad_4_digitizer_SN;
   UShort_t        ShCal_front_source_quad_4_digitizer_handle;
   UShort_t        ShCal_front_source_quad_4_channel;
   Float_t         ShCal_front_source_quad_4_vrange;
   UShort_t        ShCal_rear_source_quad_1_digitizer_SN;
   UShort_t        ShCal_rear_source_quad_1_digitizer_handle;
   UShort_t        ShCal_rear_source_quad_1_channel;
   Float_t         ShCal_rear_source_quad_1_vrange;
   UShort_t        ShCal_rear_source_quad_2_digitizer_SN;
   UShort_t        ShCal_rear_source_quad_2_digitizer_handle;
   UShort_t        ShCal_rear_source_quad_2_channel;
   Float_t         ShCal_rear_source_quad_2_vrange;
   UShort_t        ShCal_rear_source_quad_3_digitizer_SN;
   UShort_t        ShCal_rear_source_quad_3_digitizer_handle;
   UShort_t        ShCal_rear_source_quad_3_channel;
   Float_t         ShCal_rear_source_quad_3_vrange;
   UShort_t        ShCal_rear_source_quad_4_digitizer_SN;
   UShort_t        ShCal_rear_source_quad_4_digitizer_handle;
   UShort_t        ShCal_rear_source_quad_4_channel;
   Float_t         ShCal_rear_source_quad_4_vrange;
   UShort_t        FCal_rod_front_nosource_digitizer_SN;
   UShort_t        FCal_rod_front_nosource_digitizer_handle;
   UShort_t        FCal_rod_front_nosource_channel;
   Float_t         FCal_rod_front_nosource_vrange;
   UShort_t        FCal_rod_mid_nosource_digitizer_SN;
   UShort_t        FCal_rod_mid_nosource_digitizer_handle;
   UShort_t        FCal_rod_mid_nosource_channel;
   Float_t         FCal_rod_mid_nosource_vrange;
   UShort_t        FCal_rod_rear_nosource_digitizer_SN;
   UShort_t        FCal_rod_rear_nosource_digitizer_handle;
   UShort_t        FCal_rod_rear_nosource_channel;
   Float_t         FCal_rod_rear_nosource_vrange;
   UShort_t        ShCal_front_nosource_quad_1_digitizer_SN;
   UShort_t        ShCal_front_nosource_quad_1_digitizer_handle;
   UShort_t        ShCal_front_nosource_quad_1_channel;
   Float_t         ShCal_front_nosource_quad_1_vrange;
   UShort_t        ShCal_front_nosource_quad_2_digitizer_SN;
   UShort_t        ShCal_front_nosource_quad_2_digitizer_handle;
   UShort_t        ShCal_front_nosource_quad_2_channel;
   Float_t         ShCal_front_nosource_quad_2_vrange;
   UShort_t        ShCal_front_nosource_quad_3_digitizer_SN;
   UShort_t        ShCal_front_nosource_quad_3_digitizer_handle;
   UShort_t        ShCal_front_nosource_quad_3_channel;
   Float_t         ShCal_front_nosource_quad_3_vrange;
   UShort_t        ShCal_front_nosource_quad_4_digitizer_SN;
   UShort_t        ShCal_front_nosource_quad_4_digitizer_handle;
   UShort_t        ShCal_front_nosource_quad_4_channel;
   Float_t         ShCal_front_nosource_quad_4_vrange;
   UShort_t        ShCal_rear_nosource_quad_1_digitizer_SN;
   UShort_t        ShCal_rear_nosource_quad_1_digitizer_handle;
   UShort_t        ShCal_rear_nosource_quad_1_channel;
   Float_t         ShCal_rear_nosource_quad_1_vrange;
   UShort_t        ShCal_rear_nosource_quad_2_digitizer_SN;
   UShort_t        ShCal_rear_nosource_quad_2_digitizer_handle;
   UShort_t        ShCal_rear_nosource_quad_2_channel;
   Float_t         ShCal_rear_nosource_quad_2_vrange;
   UShort_t        ShCal_rear_nosource_quad_3_digitizer_SN;
   UShort_t        ShCal_rear_nosource_quad_3_digitizer_handle;
   UShort_t        ShCal_rear_nosource_quad_3_channel;
   Float_t         ShCal_rear_nosource_quad_3_vrange;
   UShort_t        ShCal_rear_nosource_quad_4_digitizer_SN;
   UShort_t        ShCal_rear_nosource_quad_4_digitizer_handle;
   UShort_t        ShCal_rear_nosource_quad_4_channel;
   Float_t         ShCal_rear_nosource_quad_4_vrange;
   UShort_t        S1_counter_digitizer_SN;
   UShort_t        S1_counter_digitizer_handle;
   UShort_t        S1_counter_channel;
   Float_t         S1_counter_vrange;
   UShort_t        S2_counter_digitizer_SN;
   UShort_t        S2_counter_digitizer_handle;
   UShort_t        S2_counter_channel;
   Float_t         S2_counter_vrange;
   UShort_t        S3_counter_digitizer_SN;
   UShort_t        S3_counter_digitizer_handle;
   UShort_t        S3_counter_channel;
   Float_t         S3_counter_vrange;
   UShort_t        S4_counter_digitizer_SN;
   UShort_t        Leakage_counter_digitizer_handle;
   UShort_t        S4_counter_channel;
   Float_t         S4_counter_vrange;
   UShort_t        Leakage_counter_digitizer_SN;
   UShort_t        Leakage_counter_channel;
   Float_t         Leakage_counter_vrange;
   UShort_t        Halo_counter_digitizer_SN;
   UShort_t        Halo_counter_digitizer_handle;
   UShort_t        Halo_counter_channel;
   Float_t         Halo_counter_vrange;
   UShort_t        Digitizer_1_trigger_digitizer_SN;
   UShort_t        Digitizer_1_trigger_digitizer_handle;
   UShort_t        Digitizer_1_trigger_channel;
   Float_t         Digitizer_1_trigger_vrange;
   UShort_t        Digitizer_2_trigger_digitizer_SN;
   UShort_t        Digitizer_2_trigger_digitizer_handle;
   UShort_t        Digitizer_2_trigger_channel;
   Float_t         Digitizer_2_trigger_vrange;
   UShort_t        Digitizer_3_trigger_digitizer_SN;
   UShort_t        Digitizer_3_trigger_digitizer_handle;
   UShort_t        Digitizer_3_trigger_channel;
   Float_t         Digitizer_3_trigger_vrange;
   UShort_t        Digitizer_4_trigger_digitizer_SN;
   UShort_t        Digitizer_4_trigger_digitizer_handle;
   UShort_t        Digitizer_4_trigger_channel;
   Float_t         Digitizer_4_trigger_vrange;
   Bool_t          good_BPC_data;
   UShort_t        BPC_tag;
   UShort_t        BPC_runNumber;
   UShort_t        BPC_spillNumber;
   UShort_t        BPC_triggerNumber;
   UInt_t          BPC_accum_time;
   UInt_t          BPC_wall_time;
   Float_t         BPC_x0;
   Float_t         BPC_y0;
   Float_t         BPC_x1;
   Float_t         BPC_y1;
   Float_t         BPC_x2;
   Float_t         BPC_y2;
   Float_t         BPC_x3;
   Float_t         BPC_y3;

  // List of branches
   TBranch        *b_dataFormatVersion;   //!
   TBranch        *b_channelDataSize;   //!
   TBranch        *b_RecordLength;   //!
   TBranch        *b_PostTrigger;   //!
   TBranch        *b_FPIOtype;   //!
   TBranch        *b_NumDigitizers;   //!
   TBranch        *b_SerialNumber;   //!
   TBranch        *b_ExtTriggerMode;   //!
   TBranch        *b_EnableMask;   //!
   TBranch        *b_DCoffset;   //!
   TBranch        *b_ChannelTriggerMode;   //!
   TBranch        *b_PulsePolarity;   //!
   TBranch        *b_GWn;   //!
   TBranch        *b_GWaddr;   //!
   TBranch        *b_GWdata;   //!
   TBranch        *b_GWmask;   //!
   TBranch        *b_nCryostats;   //!
   TBranch        *b_scintUsed;   //!
   TBranch        *b_includeTailVeto;   //!
   TBranch        *b_sourceInBeam;   //!
   TBranch        *b_runNumber;   //!
   TBranch        *b_spillNumber;   //!
   TBranch        *b_spillStartTime;   //!
   TBranch        *b_BeamStatus;   //!
   TBranch        *b_boardFailBits;   //!
   TBranch        *b_SCal_HV_set;   //!
   TBranch        *b_SCal_HV_read;   //!
   TBranch        *b_FCal_inner_HV_set;   //!
   TBranch        *b_FCal_tubeA_HV_read;   //!
   TBranch        *b_FCal_outer_HV_set;   //!
   TBranch        *b_FCal_tubeB_HV_read;   //!
   TBranch        *b_FCal_tubeC_HV_read;   //!
   TBranch        *b_NumEventsMax;   //!
   TBranch        *b_NumEventsInSpill;   //!
   TBranch        *b_nchannels;   //!
   TBranch        *b_eventCounter;   //!
   TBranch        *b_triggerTimeTag;   //!
   TBranch        *b_FCal_rod_front_source_data;   //!
   TBranch        *b_FCal_rod_mid_source_data;   //!
   TBranch        *b_FCal_rod_rear_source_data;   //!
   TBranch        *b_ShCal_front_source_quad_1_data;   //!
   TBranch        *b_ShCal_front_source_quad_2_data;   //!
   TBranch        *b_ShCal_front_source_quad_3_data;   //!
   TBranch        *b_ShCal_front_source_quad_4_data;   //!
   TBranch        *b_ShCal_rear_source_quad_1_data;   //!
   TBranch        *b_ShCal_rear_source_quad_2_data;   //!
   TBranch        *b_ShCal_rear_source_quad_3_data;   //!
   TBranch        *b_ShCal_rear_source_quad_4_data;   //!
   TBranch        *b_FCal_rod_front_nosource_data;   //!
   TBranch        *b_FCal_rod_mid_nosource_data;   //!
   TBranch        *b_FCal_rod_rear_nosource_data;   //!
   TBranch        *b_ShCal_front_nosource_quad_1_data;   //!
   TBranch        *b_ShCal_front_nosource_quad_2_data;   //!
   TBranch        *b_ShCal_front_nosource_quad_3_data;   //!
   TBranch        *b_ShCal_front_nosource_quad_4_data;   //!
   TBranch        *b_ShCal_rear_nosource_quad_1_data;   //!
   TBranch        *b_ShCal_rear_nosource_quad_2_data;   //!
   TBranch        *b_ShCal_rear_nosource_quad_3_data;   //!
   TBranch        *b_ShCal_rear_nosource_quad_4_data;   //!
   TBranch        *b_S1_counter_data;   //!
   TBranch        *b_S2_counter_data;   //!
   TBranch        *b_S3_counter_data;   //!
   TBranch        *b_S4_counter_data;   //!
   TBranch        *b_Leakage_counter_data;   //!
   TBranch        *b_Halo_counter_data;   //!
   TBranch        *b_Digitizer_1_trigger_data;   //!
   TBranch        *b_Digitizer_2_trigger_data;   //!
   TBranch        *b_Digitizer_3_trigger_data;   //!
   TBranch        *b_Digitizer_4_trigger_data;   //!
   TBranch        *b_FCal_rod_front_source_digitizer_SN;   //!
   TBranch        *b_FCal_rod_front_source_digitizer_handle;   //!
   TBranch        *b_FCal_rod_front_source_channel;   //!
   TBranch        *b_FCal_rod_front_source_vrange;   //!
   TBranch        *b_FCal_rod_mid_source_digitizer_SN;   //!
   TBranch        *b_FCal_rod_mid_source_digitizer_handle;   //!
   TBranch        *b_FCal_rod_mid_source_channel;   //!
   TBranch        *b_FCal_rod_mid_source_vrange;   //!
   TBranch        *b_FCal_rod_rear_source_digitizer_SN;   //!
   TBranch        *b_FCal_rod_rear_source_digitizer_handle;   //!
   TBranch        *b_FCal_rod_rear_source_channel;   //!
   TBranch        *b_FCal_rod_rear_source_vrange;   //!
   TBranch        *b_ShCal_front_source_quad_1_digitizer_SN;   //!
   TBranch        *b_ShCal_front_source_quad_1_digitizer_handle;   //!
   TBranch        *b_ShCal_front_source_quad_1_channel;   //!
   TBranch        *b_ShCal_front_source_quad_1_vrange;   //!
   TBranch        *b_ShCal_front_source_quad_2_digitizer_SN;   //!
   TBranch        *b_ShCal_front_source_quad_2_digitizer_handle;   //!
   TBranch        *b_ShCal_front_source_quad_2_channel;   //!
   TBranch        *b_ShCal_front_source_quad_2_vrange;   //!
   TBranch        *b_ShCal_front_source_quad_3_digitizer_SN;   //!
   TBranch        *b_ShCal_front_source_quad_3_digitizer_handle;   //!
   TBranch        *b_ShCal_front_source_quad_3_channel;   //!
   TBranch        *b_ShCal_front_source_quad_3_vrange;   //!
   TBranch        *b_ShCal_front_source_quad_4_digitizer_SN;   //!
   TBranch        *b_ShCal_front_source_quad_4_digitizer_handle;   //!
   TBranch        *b_ShCal_front_source_quad_4_channel;   //!
   TBranch        *b_ShCal_front_source_quad_4_vrange;   //!
   TBranch        *b_ShCal_rear_source_quad_1_digitizer_SN;   //!
   TBranch        *b_ShCal_rear_source_quad_1_digitizer_handle;   //!
   TBranch        *b_ShCal_rear_source_quad_1_channel;   //!
   TBranch        *b_ShCal_rear_source_quad_1_vrange;   //!
   TBranch        *b_ShCal_rear_source_quad_2_digitizer_SN;   //!
   TBranch        *b_ShCal_rear_source_quad_2_digitizer_handle;   //!
   TBranch        *b_ShCal_rear_source_quad_2_channel;   //!
   TBranch        *b_ShCal_rear_source_quad_2_vrange;   //!
   TBranch        *b_ShCal_rear_source_quad_3_digitizer_SN;   //!
   TBranch        *b_ShCal_rear_source_quad_3_digitizer_handle;   //!
   TBranch        *b_ShCal_rear_source_quad_3_channel;   //!
   TBranch        *b_ShCal_rear_source_quad_3_vrange;   //!
   TBranch        *b_ShCal_rear_source_quad_4_digitizer_SN;   //!
   TBranch        *b_ShCal_rear_source_quad_4_digitizer_handle;   //!
   TBranch        *b_ShCal_rear_source_quad_4_channel;   //!
   TBranch        *b_ShCal_rear_source_quad_4_vrange;   //!
   TBranch        *b_FCal_rod_front_nosource_digitizer_SN;   //!
   TBranch        *b_FCal_rod_front_nosource_digitizer_handle;   //!
   TBranch        *b_FCal_rod_front_nosource_channel;   //!
   TBranch        *b_FCal_rod_front_nosource_vrange;   //!
   TBranch        *b_FCal_rod_mid_nosource_digitizer_SN;   //!
   TBranch        *b_FCal_rod_mid_nosource_digitizer_handle;   //!
   TBranch        *b_FCal_rod_mid_nosource_channel;   //!
   TBranch        *b_FCal_rod_mid_nosource_vrange;   //!
   TBranch        *b_FCal_rod_rear_nosource_digitizer_SN;   //!
   TBranch        *b_FCal_rod_rear_nosource_digitizer_handle;   //!
   TBranch        *b_FCal_rod_rear_nosource_channel;   //!
   TBranch        *b_FCal_rod_rear_nosource_vrange;   //!
   TBranch        *b_ShCal_front_nosource_quad_1_digitizer_SN;   //!
   TBranch        *b_ShCal_front_nosource_quad_1_digitizer_handle;   //!
   TBranch        *b_ShCal_front_nosource_quad_1_channel;   //!
   TBranch        *b_ShCal_front_nosource_quad_1_vrange;   //!
   TBranch        *b_ShCal_front_nosource_quad_2_digitizer_SN;   //!
   TBranch        *b_ShCal_front_nosource_quad_2_digitizer_handle;   //!
   TBranch        *b_ShCal_front_nosource_quad_2_channel;   //!
   TBranch        *b_ShCal_front_nosource_quad_2_vrange;   //!
   TBranch        *b_ShCal_front_nosource_quad_3_digitizer_SN;   //!
   TBranch        *b_ShCal_front_nosource_quad_3_digitizer_handle;   //!
   TBranch        *b_ShCal_front_nosource_quad_3_channel;   //!
   TBranch        *b_ShCal_front_nosource_quad_3_vrange;   //!
   TBranch        *b_ShCal_front_nosource_quad_4_digitizer_SN;   //!
   TBranch        *b_ShCal_front_nosource_quad_4_digitizer_handle;   //!
   TBranch        *b_ShCal_front_nosource_quad_4_channel;   //!
   TBranch        *b_ShCal_front_nosource_quad_4_vrange;   //!
   TBranch        *b_ShCal_rear_nosource_quad_1_digitizer_SN;   //!
   TBranch        *b_ShCal_rear_nosource_quad_1_digitizer_handle;   //!
   TBranch        *b_ShCal_rear_nosource_quad_1_channel;   //!
   TBranch        *b_ShCal_rear_nosource_quad_1_vrange;   //!
   TBranch        *b_ShCal_rear_nosource_quad_2_digitizer_SN;   //!
   TBranch        *b_ShCal_rear_nosource_quad_2_digitizer_handle;   //!
   TBranch        *b_ShCal_rear_nosource_quad_2_channel;   //!
   TBranch        *b_ShCal_rear_nosource_quad_2_vrange;   //!
   TBranch        *b_ShCal_rear_nosource_quad_3_digitizer_SN;   //!
   TBranch        *b_ShCal_rear_nosource_quad_3_digitizer_handle;   //!
   TBranch        *b_ShCal_rear_nosource_quad_3_channel;   //!
   TBranch        *b_ShCal_rear_nosource_quad_3_vrange;   //!
   TBranch        *b_ShCal_rear_nosource_quad_4_digitizer_SN;   //!
   TBranch        *b_ShCal_rear_nosource_quad_4_digitizer_handle;   //!
   TBranch        *b_ShCal_rear_nosource_quad_4_channel;   //!
   TBranch        *b_ShCal_rear_nosource_quad_4_vrange;   //!
   TBranch        *b_S1_counter_digitizer_SN;   //!
   TBranch        *b_S1_counter_digitizer_handle;   //!
   TBranch        *b_S1_counter_channel;   //!
   TBranch        *b_S1_counter_vrange;   //!
   TBranch        *b_S2_counter_digitizer_SN;   //!
   TBranch        *b_S2_counter_digitizer_handle;   //!
   TBranch        *b_S2_counter_channel;   //!
   TBranch        *b_S2_counter_vrange;   //!
   TBranch        *b_S3_counter_digitizer_SN;   //!
   TBranch        *b_S3_counter_digitizer_handle;   //!
   TBranch        *b_S3_counter_channel;   //!
   TBranch        *b_S3_counter_vrange;   //!
   TBranch        *b_S4_counter_digitizer_SN;   //!
   TBranch        *b_Leakage_counter_digitizer_handle;   //!
   TBranch        *b_S4_counter_channel;   //!
   TBranch        *b_S4_counter_vrange;   //!
   TBranch        *b_Leakage_counter_digitizer_SN;   //!
   TBranch        *b_Leakage_counter_channel;   //!
   TBranch        *b_Leakage_counter_vrange;   //!
   TBranch        *b_Halo_counter_digitizer_SN;   //!
   TBranch        *b_Halo_counter_digitizer_handle;   //!
   TBranch        *b_Halo_counter_channel;   //!
   TBranch        *b_Halo_counter_vrange;   //!
   TBranch        *b_Digitizer_1_trigger_digitizer_SN;   //!
   TBranch        *b_Digitizer_1_trigger_digitizer_handle;   //!
   TBranch        *b_Digitizer_1_trigger_channel;   //!
   TBranch        *b_Digitizer_1_trigger_vrange;   //!
   TBranch        *b_Digitizer_2_trigger_digitizer_SN;   //!
   TBranch        *b_Digitizer_2_trigger_digitizer_handle;   //!
   TBranch        *b_Digitizer_2_trigger_channel;   //!
   TBranch        *b_Digitizer_2_trigger_vrange;   //!
   TBranch        *b_Digitizer_3_trigger_digitizer_SN;   //!
   TBranch        *b_Digitizer_3_trigger_digitizer_handle;   //!
   TBranch        *b_Digitizer_3_trigger_channel;   //!
   TBranch        *b_Digitizer_3_trigger_vrange;   //!
   TBranch        *b_Digitizer_4_trigger_digitizer_SN;   //!
   TBranch        *b_Digitizer_4_trigger_digitizer_handle;   //!
   TBranch        *b_Digitizer_4_trigger_channel;   //!
   TBranch        *b_Digitizer_4_trigger_vrange;   //!
   TBranch        *b_good_BPC_data;   //!
   TBranch        *b_BPC_tag;   //!
   TBranch        *b_BPC_runNumber;   //!
   TBranch        *b_BPC_spillNumber;   //!
   TBranch        *b_BPC_triggerNumber;   //!
   TBranch        *b_BPC_accum_time;   //!
   TBranch        *b_BPC_wall_time;   //!
   TBranch        *b_BPC_x0;   //!
   TBranch        *b_BPC_y0;   //!
   TBranch        *b_BPC_x1;   //!
   TBranch        *b_BPC_y1;   //!
   TBranch        *b_BPC_x2;   //!
   TBranch        *b_BPC_y2;   //!
   TBranch        *b_BPC_x3;   //!
   TBranch        *b_BPC_y3;   //!

   int SetBranchAddress();

};

#endif
