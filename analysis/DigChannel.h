#ifndef DIGCHANNEL_H
#define DIGCHANNEL_H
#include "TF1.h"
#include "TProfile.h"
#include "TGraphErrors.h"
#include "TFitResultPtr.h"
#include <math.h>

class DigChannel {
//--------------------------------------
 private:
//--------------------------------------

  bool source;
  int Index;
  int HVboardINDX;
  int HVboardPIN;
  int HVboardPulsedINDX;
  int HVboardPulsedPIN;
  int Imod;  // 0,3 = tubes; 1,4 = SCAL-F; 2,5 = SCAL-R; 6 = Beam; 7 = trigger;
  int Ichan; // 0 = A;     0 = Q1;                 0 = S1;   4 = Hole/Halo
  // 1 = B;     1 = Q2                  1 = S2;   5 = Leak;
  // 2 = C;     2 = Q3;                 2 = S3;   6 = Trigger;
  //            3 = Q4;                 3 = S4;
  int Idig;          // digitizer #
  int InDig;         // digitizer input #
  int iSerialNumber; // Digitizer Serial Number
  int ImodPulsed;
  int IchanPulsed;
  int Irange0; // signal range, samples;
  int Irange1;
  int Irange0S; // special integral range, samples. Used for S3 tail, for example;
  int Irange1S;
  int PedRange; // last sample for pedestal calculations
  int nSamp;    // number of data samples;
  int DCoffset;
  double tSamp;   // time sample
  double Trange0; // signal range, ns
  double Trange1;
  double Trange0S; // signal range, ns
  double Trange1S;
  double pedMEAN; 
  double pedMEAN0;
  double pedRMS0;
  double pedRMS;
  // stats, average and rms of data in ADC 
  double sp0[4];  // 0 - before integration range
  double sp1[4];  // 1 - in integration range
  double sp2[4];  // 2 - after integration range
  // 3 - after removing "hot" areas
  double pStat;   // statistics for pedestal calculation;
  double aMAX;
  double aMIN;
  double localMAX;
  double localMIN;
  
  int    iOptFitFront;
  double tFront;
  double tFrontFit;
  bool   addTrigTime;
  int    NextraSamples;
  double trigFront; // front time for digitized trigger in this module, set after trigger was processed
  // and ComputeHalfPulse() is called;
  int    trigSample;// trigger sample
  double triggerSlope;
  double a0trig;
  double a1trig;
  double aRangeTrig;
  double aClosest;
  int    iClosest;
  
  double Vrange;
  double ADCtoMV;   // calibration constants
  double MVtoPC;
  double ADCtoMIP;
  double ADCtoPC;
  double PCtoMIP;
  
  double HVread;
  
  double SPECIALintegral; // special integral, ADC;
  double RAWintegral;     // integral, ADC in predefined interval
  double RAWintegralT0T1; // integral, ADC in interval T0, T1
  double RMSint;          // RMS for integral-sized integrals, taken BEFORE the signal ADC;
  
  double cut;     // used in search for the leading edge;
  bool   invert;  // used in search for leading edge and integral calculation
  
  std::vector<unsigned short> *Raw;
  std::vector<double>          ampD; // amplitude, ADC, pedestal subtracted;
  std::vector<unsigned short>  foundSignals0; // signal start
  std::vector<unsigned short>  foundSignals1; // signal end
  std::vector<unsigned short>  foundIpeak;    // peak Index
  std::vector<double>          foundApeak;    // peak Amplitude (ADC)
  std::vector<double>          foundS1peak;   // average Amplitude in peak zone (ADC)
  std::vector<double>          foundS2peak;   //   sigma Amplitude in peak zone (ADC)
  std::vector<double>          foundPCpeak;   //   peak integral (pC)
  std::vector<double>          foundTimeRMS;  //   time spread
  
  std::vector<int>             firstOf100;
  std::vector<double>          numbrOf100;
  std::vector<double>          average100;
  std::vector<double>          sigmaOf100;
  double sigma100cut;
  
  char myName[200];
  char myNamePulsed[200];
  std::string my_Name;
  std::string my_Name_Pulsed;
  
  const double fall_cut[8]={ 20.0, 20.0, 20.0, 20.0, 20.0, 20.0, 7.50, 7.50}; 
  const int    countMIN[8]={    5,  100,  100,    5,  100,  100,    5,    5};
  const int    dropsMAX[8]={    6,    6,    6,    6,    6,    6,    2,    5};
  const double rmsTOavr[8]={  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.4,  0.4}; 
  const double  peakMIN[8]={  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  10.,  10.}; 
  const double     qMIN[8]={  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  10.,  10.}; 
  //const double scintCut[6]={ 30.0, 30.0, 30.0, 30.0,  5.0, 30.0};
  //const double pastRMScutScint[6]={6.0, 3.0, 3.0, 3.0, 5.0, 3.0};
  //#include "Cuts.inc.C"
    TProfile ZeroShape;
    bool SubtractZeroShape;

    TF1*          fitFront1;
    TF1*          fitFront;
    TGraphErrors* FitTrigger;
    TFitResultPtr myFit;

    const double scintCut[6]={ 30.0, 30.0, 30.0, 30.0,  5.0, 30.0};
    const double pastRMScutScint[6]={5.0, 2.0, 2.0, 2.0, 2.0, 2.0};
    const int    scintCutOffset[6]={0, 0, -2, -3, -3, -3}; // offset in the "sigmaOf100" arrays

//--------------
// pusle front paramtrization
    static Double_t funcFront(Double_t *x, Double_t *par);
// pusle front paramtrization v2
// assuming leadint edge par[1]
    static Double_t funcFront1(Double_t *x, Double_t *par);

//--------------------------------------
public:
//--------------------------------------
    DigChannel();
    void Clear();
    void SetSubtractZero(bool set) {SubtractZeroShape=set;}
    void SetSource(bool s) {source=s;}
    bool GetSource()       {return source;}
//--------------------------------------
// version with trigger pulse and digitizer SN options 
// 2022 testbeam
    void Set(int ix, int iDig, int iSN, int iIn, int im, int ich, bool s, int trigOpt);



//--------------------------------------
// version with trigger pulse options 
//
    void Set(int ix, int iDig, int iIn, int im, int ich, bool s, double t, int trigOpt);


//--------------------------------------
    void Set(int ix, int im, int ich, bool s, double t);

//set channel name based on HV board # and intput pin #
    std::string GetMyName(int hvb, int pin);

//--------------------------------------
// set channel name based on HV board # and intput pin #
    void SetName(int hvRead, int ic);
//--------------------------------------
// set channel name based on HV board # and intput pin #
    void SetNamePulsed(int hvPulsed, int pin); 

//--------------------------------------
// special setting for Xtalk study
    void SetXtalk(int ix, int im, int ich, bool s, double t);

// set zero ringing data
    void SetZeroShape(TProfile zShape);

//----------------------------
void SetDCoffset(UInt_t DC) {DCoffset=(int)DC;}
int  GetDCoffset() {return DCoffset;}
// Vrange from data added here
 void SetData(double HV, std::vector<unsigned short> *data, UInt_t Vr);
//--------------------------------------
// Vrange from data added here
 void SetData(int id, int in, double HV, std::vector<unsigned short> *data, UInt_t Vr);
//--------------------------------------
// original
 void SetData(int id, int in, double HV, std::vector<unsigned short> *data);

//--------------------------------------
// OLD set - with data 
 void Set(int id, int in, int im, int ich, std::vector<unsigned short> *data, bool s, double t);
//--------------------------------------
 void ComputeSpecialRMS();
//--------------------------------------
 void SignalPreprocess();
//==================================================
 void CountSignals();

//......................................
 void PrintPulses();

//......................................
int GetNfoundSignals() {return foundSignals0.size();}
//-------------------------
 double GetT2peak(int is);

//-------------------------
 int GetIpeak(int is);

//-------------------------
 double GetS1peak(int is);
//-------------------------
 double GetS2peak(int is);
//-------------------------
 double GetApeak(int is);

 double GetApeakMV(int is) {
   return ADCtoMV*GetApeak(is); 
 }
 //-------------------------
 double GetQpeak(int is);
//-------------------------
 int GetIfoundSignal(int indx, int is);

//--------------------------------------
 void SetTriggerFitOption(int iOpt) {iOptFitFront=iOpt;}
 void SetTriggerSlope(double s)     {triggerSlope=s;}
 //--------------------------------------

//==================================================
 void ComputeHalfPulse();
 
//--------------------------------------
 void   SetTrigTime(double t);
 double GetTrigTime()         {return trigFront;}
 int    GetTrigSample()       {return trigSample;}
//--------------------------------------
 void SetRangesCalib2022(double tpr, double tr0, double tr1, double ts0, double ts1, double c);

//--------------------------------------
 void SetRangesCalib(int pr, int ir0, int ir1, int is0, int is1, double c);

 //--------------------------------------
 void SetRangesCalib(double tpr, double tr0, double tr1, double ts0, double ts1, double c);
 //--------------------------------------
 void ComputeIntegral(double t0, double t1);

//--------------------------------------
 void ComputeIntegral();

 //--------------------------------------
 int  GetHVboardINDX() {return HVboardINDX;}
 int  GetHVboardPIN()  {return HVboardPIN;}
 int  GetHVboardPulsedINDX() {return HVboardPulsedINDX;}
 int  GetHVboardPulsedPIN()  {return HVboardPulsedPIN;}
 //--------------------------------------
 int  GetDigitizer() {return Idig;}
 int  GetDigInput()  {return InDig;}
 int  GetModule()    {return Imod;}
 int  GetChannelPulsed()   {return IchanPulsed;}
 int  GetModulePulsed()    {return ImodPulsed;}
 int  GetChannel()   {return Ichan;}
 bool CheckSource()  {return source;}
 bool CheckIvert()   {return invert;}
 double GetTimingCut() {return cut*ADCtoMV;}
 double GetPolarity();

//--------------------------------------
 double GetnSamp()     {return Raw->size();} // number of samples
 double GetTsamp()     {return tSamp;}
 double GetHalfPulse() {return tFront;}
 double GetFitFront()  {return tFrontFit;}
 double GetHV()        {return HVread;}
 //--------------------------------------
 // trigger related getters
 double GetA0trig()  {return a0trig;} // sample before half
 double GetA1trig()  {return a1trig;} // sample after half
 double GetArange()  {return aRangeTrig;} // amplitude
 double GetArangeMV(){return aRangeTrig*ADCtoMV;} // amplitude
 double GetAclosest(){return aClosest;} // closest amplitude to 1/2
 int    GetIclosest(){return iClosest;} // closest index to 1/2
 //--------------------------------------
 double GetTrange0() {return Trange0;}
 double GetTrange1() {return Trange1;}
 double GetTcenter() {return 0.5*(Trange0+Trange1);}
 int    GetIrange0() {return Irange0;}
 int    GetIrange1() {return Irange1;}
 //--------------------------------------
 double GetIntegralRaw()   { return RAWintegral; }
 double GetIntegralMV()    { return RAWintegral*ADCtoMV;  }
 double GetIntegralPC()    { return RAWintegral*ADCtoPC;  }
 double GetIntegralCalib() { return RAWintegral*ADCtoMIP; }
 //--------------------------------------
 double GetIntegralT0T1Raw()   { return RAWintegralT0T1; }
 double GetIntegralT0T1MV()    { return RAWintegralT0T1*ADCtoMV;  }
 double GetIntegralT0T1PC()    { return RAWintegralT0T1*ADCtoPC;  }
 double GetIntegralT0T1Calib() { return RAWintegralT0T1*ADCtoMIP; }
 //--------------------------------------
 double GetSPECIALintegral()    { return SPECIALintegral; }
 double GetSPECIALintegralMV()  { return SPECIALintegral*ADCtoMV;  }
 double GetSPECIALintegralPC()  { return SPECIALintegral*ADCtoPC;  }
 double GetSPECIALintegralMIP() { return SPECIALintegral*ADCtoMIP; }
//--------------------------------------
 double GetIntegralRaw(double t0trig, double t1trig);
 
 double GetIntegralMV(double t0trig, double t1trig)    { return GetIntegralRaw(t0trig, t1trig)*ADCtoMV;  }
 double GetIntegralPC(double t0trig, double t1trig)    { return GetIntegralRaw(t0trig, t1trig)*ADCtoPC;  }
 double GetIntegralCalib(double t0trig, double t1trig) { return GetIntegralRaw(t0trig, t1trig)*ADCtoMIP; }
//--------------------------------------
double GetRawNoise()   { return pedRMS; }
double GetMVNoise()    { return pedRMS*abs(ADCtoMV); }
//--------------------------------------
double GetRawNoiseInt()   { return RMSint; }
double GetMVNoiseInt()    { return RMSint*abs(ADCtoMV); }
double GetPCNoiseInt()    { return RMSint*abs(ADCtoPC); }
double GetCalibNoiseInt() { return RMSint*abs(ADCtoMIP); }
//--------------------------------------
double GetPedADC()        {return pedMEAN;}
double GetPedMV()         {return pedMEAN*abs(ADCtoMV);}
//--------------------------------------
double GetRMSxADC(int i)  {return sp2[i];}
double GetRMSxMV(int i)   {return sp2[i]*abs(ADCtoMV);}
double GetMEANxADC(int i) {return sp1[i];}
double GetMEANxMV(int i)  {return sp1[i]*abs(ADCtoMV);}
double GetERRxADC(int i)  {return sp2[i]/sqrt(sp0[i]);}
double GetERRxMV(int i)   {return sp2[i]/sqrt(sp0[i])*abs(ADCtoMV);}

//--------------------------------------
double GetMaxRaw()   { return aMAX; }
double GetMaxADC()   { return aMAX; }
double GetMaxMV()    { return aMAX*ADCtoMV; }
double GetMaxPC()    { return aMAX*ADCtoPC; }
double GetMaxCalib() { return aMAX*ADCtoMIP; }
//--------------------------------------
double GetMinRaw()   { return aMIN; }
double GetMinADC()   { return aMIN; }
double GetMinMV()    { return aMIN*ADCtoMV; }
double GetMinPC()    { return aMIN*ADCtoPC; }
double GetMinCalib() { return aMIN*ADCtoMIP; }

//--------------------------------------
 double GetSampleTime(int i);

 double GetSampleTimeTrig(int i);

//--------------------------------------
 double GetAverageRaw(double t0, double t1);
 
 double GetAverageADC(double t0, double t1)   {
   return GetAverageRaw(t0, t1);
 }
 double GetAverageMV(double t0, double t1)   {
   return GetAverageRaw(t0, t1)*ADCtoMV;
 }

//--------------------------------------
 double GetAverageTimedRaw(double t0, double t1);
 
 double GetAverageTimedADC(double t0, double t1)   {
   return GetAverageTimedRaw(t0, t1);
 }
 double GetAverageTimedMV(double t0, double t1)   {
   return GetAverageTimedRaw(t0, t1)*ADCtoMV;
 }

//--------------------------------------
 double GetMaxLocalRaw(double t0, double t1);

 double GetMaxLocalADC(double t0, double t1)   {
   return GetMaxLocalRaw(t0, t1);
 }
 double GetMaxLocalMV(double t0, double t1)   {
   return GetMaxLocalRaw(t0, t1)*ADCtoMV;
 }

//--------------------------------------
 double GetMinLocalRaw(double t0, double t1);

 double GetMinLocalADC(double t0, double t1)   {
   return GetMinLocalRaw(t0, t1);
 }
 double GetMinLocalMV(double t0, double t1)   {
   return GetMinLocalRaw(t0, t1)*ADCtoMV;
 }

//--------------------------------------
 double GetMaxLocalTimedRaw(double t0, double t1); 

 double GetMaxLocalTimedADC(double t0, double t1)   {
   return GetMaxLocalTimedRaw(t0, t1);
 }
 double GetMaxLocalTimedMV(double t0, double t1)   {
   return GetMaxLocalTimedRaw(t0, t1)*ADCtoMV;
 }
//--------------------------------------
 double GetMinLocalTimedRaw(double t0, double t1);

 double GetMinLocalTimeADC(double t0, double t1)   {
   return GetMinLocalTimedRaw(t0, t1);
 }
 double GetMinLocalTimeMV(double t0, double t1)   {
   return GetMinLocalTimedRaw(t0, t1)*ADCtoMV;
 }
//--------------------------------------
//--------------------------------------
 std::string GetName();
//--------------------------------------
 std::string GetNamePulsed();

//--------------------------------------
 unsigned short GetRaw(int i);
//--------------------------------------
 double GetADC(int i);
 double GetMV(int i);

 double GetPC(int i);
 double GetCalib(int i);
 int GetSample(double t);
 int GetSampleTrig(double t);

//--------------------------------------
double GetSpecialIntegral() {
  return SPECIALintegral;
}


//--------------------------------------
 int GetPedIntervalsN() {return (int)firstOf100.size();}
 int GetFirstOf100(int i);

 double GetAverage100(int i); 
 double GetIntegral100(int i);
 double GetSigmaOf100(int i);

 double GetAverage100mv(int i) {
   return abs(ADCtoMV)*this->GetAverage100(i);
 }
 double GetSigmaOf100mv(int i) {
   return abs(ADCtoMV)*this->GetSigmaOf100(i);
 }
 double GetIntegral100pc(int i) {
   return abs(ADCtoPC)*this->GetIntegral100(i);
 }
//--------------------------------------


 void Print();

//--------------------------------------
 void Print1(int iPr);
 
//--------------------------------------
 void PrintInfo();
//--------------------------------------
 void PrintInfo1();

 void PrintInfo1(int iPr);

//--------------------------------------

};

#endif


