#include "Dig_utils.h"
#include "TH1F.h"
#include "TFile.h"
#include "Rtypes.h"
#include "TCanvas.h"
#include "TF1.h"
#include "TGraphErrors.h"
#include <iostream>


#define nHV_vals 17

TGraph *g;

float area_error = 30.;  // just a guess at the moment

int main() {

  TCanvas *c1 = new TCanvas();

  Dig_utils Digu;

  char hname[100];
  float HV_vals[nHV_vals] = {300, 250, 200, 150, 130, 120, 110, 100, 90, 80, 70, 60, 50, 40, 30, 20, 10};
  float ratio[nHV_vals], ratio_error[nHV_vals], HV_error[nHV_vals];

  TH1F* hSr90in;
  TH1F* hSr90out;

  TFile fin("avgPulses.root");
  Float_t area_Sr90in, area_Sr90out;

  std::cout << "All SCal triggers" << std::endl;

  for (int iHV = 0; iHV<nHV_vals; iHV++) {
    sprintf(hname, "FCal_mid_d%d_Sr90in", int(HV_vals[iHV]));
    hSr90in = (TH1F*)fin.Get(hname);
    area_Sr90in = Digu.GetArea(hSr90in);

    sprintf(hname, "FCal_mid_d%d_Sr90out", int(HV_vals[iHV]));
    hSr90out = (TH1F*)fin.Get(hname);
    area_Sr90out = Digu.GetArea(hSr90out);

    std::cout << HV_vals[iHV] << ": " << area_Sr90in << " " << area_Sr90out << " " << area_Sr90in/area_Sr90out << std::endl;

    ratio[iHV] = area_Sr90in/area_Sr90out;
    ratio_error[iHV] = TMath::Sqrt(area_error*area_error/(area_Sr90out*area_Sr90out) + (area_Sr90in*area_Sr90in*area_error*area_error/(area_Sr90out*area_Sr90out*area_Sr90out*area_Sr90out) ));
    std::cout << ratio_error[iHV] << std::endl;
    HV_error[iHV]=0;				   
				   
  }

  std::cout << "Lower half SCal triggers" << std::endl;

  TFile fin2("avgPulses_SCal23.root");

  for (int iHV = 0; iHV<nHV_vals; iHV++) {
    sprintf(hname, "FCal_mid_d%d_Sr90in", int(HV_vals[iHV]));
    hSr90in = (TH1F*)fin2.Get(hname);
    area_Sr90in = Digu.GetArea(hSr90in);

    sprintf(hname, "FCal_mid_d%d_Sr90out", int(HV_vals[iHV]));
    hSr90out = (TH1F*)fin2.Get(hname);
    area_Sr90out = Digu.GetArea(hSr90out);

    std::cout << HV_vals[iHV] << ": " << area_Sr90in << " " << area_Sr90out << " " << area_Sr90in/area_Sr90out << std::endl;
  }

  std::cout << "Upper half SCal triggers" << std::endl;

  TFile fin3("avgPulses_SCal14.root");

  for (int iHV = 0; iHV<nHV_vals; iHV++) {
    sprintf(hname, "FCal_mid_d%d_Sr90in", int(HV_vals[iHV]));
    hSr90in = (TH1F*)fin3.Get(hname);
    area_Sr90in = Digu.GetArea(hSr90in);

    sprintf(hname, "FCal_mid_d%d_Sr90out", int(HV_vals[iHV]));
    hSr90out = (TH1F*)fin3.Get(hname);
    area_Sr90out = Digu.GetArea(hSr90out);

    std::cout << HV_vals[iHV] << ": " << area_Sr90in << " " << area_Sr90out << " " << area_Sr90in/area_Sr90out << std::endl;
  }

  TF1 areaFunc("areaFunc", Digu.ExpectedPulseArea, 0, 300, 2);
  areaFunc.SetParameter(0, 1.3);
  areaFunc.SetParameter(1, 50.);

  g = new TGraphErrors(nHV_vals, HV_vals, ratio, HV_error, ratio_error);
  g->SetTitle(0);
  g->SetMarkerStyle(21);
  g->GetXaxis()->SetTitle("HV [V]");
  g->GetYaxis()->SetTitle("Avg pulse area with Sr90/Avg pulse area without Sr90");
  g->SetMinimum(0);
  g->Draw("AP");
  g->Fit(&areaFunc, "", "", 0, 65);
  //  areaFunc.Draw("SAME");
  c1->Print("ratio.pdf");
}
