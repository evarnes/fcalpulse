/*
Write out text files with fraction of the total pulse height in time bins
*/

#include "pulseFractionWriter.h"
#include "TChain.h"
#include "TH1F.h"
#include <fstream>

#define nHV_vals 17
//#define nHV_vals 1

void writePulseFractions(Int_t HV)  {

  std::cout << "Starting with HV = " << HV << std::endl;
  pulseFractionWriter pw(HV, kTRUE); 
  pw.Loop();

}

int main() {

  int HV_vals[nHV_vals] = {300, 250, 200, 150, 130, 120, 110, 100, 90, 80, 70, 60, 50, 40, 30, 20, 10};
  //int HV_vals[nHV_vals] = {300};

  for (int iHV = 0; iHV < nHV_vals; iHV++) {

    writePulseFractions(HV_vals[iHV]);
  }

}
  

