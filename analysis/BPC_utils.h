#ifndef BPC_utils_h
#define BPC_utils_h
#include <Rtypes.h>
//#include "BPC_Tracker.h"

class Digitizer_leaves;

class BPC_Tracker;

//////////////////////////////////////////////////////////
// Class containing utility functions for BPC data handling
// (much of the code was originated by A. Savine )
/////////////////////////////////////////////////////////

class BPC_utils {

 public:

  BPC_utils();

  // The following function assumed that a digitizer event has already
  // been loaded into memory, and that the corresponding full BPC data file for
  // the run has been opened.  Timestamps are used to match the digitizer and
  // BPC data.  Returns false if no match can be found (in that case the 
  // digitizer event should be skipped if BPC information is required in the
  // analysis 
  bool readMatchingBPCEvent(Long64_t &digEntry, Digitizer_leaves *digL, BPC_Tracker* myTracker);

  int nBPC;
  Double_t BPC_Z[4]; // BPC Z-coord., sum(X i)=0 (mm)
  Double_t Z0;
  Double_t Z_S4;                   // Z-coord of S4 (mm)
  Double_t Z_Hole;                    // Z-coord of Hole (mm)
  Double_t Z_S3;                     // Z-coord of S3 (mm)
  Double_t Z_S2;                       // Z-coord of S2 (mm)
  Double_t Z_S1;                      // Z-coord of S1 (mm)
  Double_t dX[4];  // computed BPX XY offsets (mm)
  Double_t dY[4];
  Double_t s_z2;
  Double_t positionCut;     // BPC position cut, +/-mm
  Double_t BPCdisplayRangeNarrow; // BPC display range +/-mm for narrow
  Double_t BPCdisplayRangeWide;   // BPC display range +/-mm for wide beam
  Double_t BPCdispRangeN; // BPC display range +/-mm for narrow beam
  Double_t BPCdispRangeW;   // BPC display range +/-mm for wide beam

  double sigBPC[4];

  double Z_TUBE[3];
      
  double w00[4];
  double ss1[5]; // average Z   i=0, 1, 2, 3 fit without BPC#i 
               //             i=4          fit all 4 BPCs
  double ss2[5]; // average Z^2
  double ssC[5]; // sum (Z-<Z>)^2*W
  double ssB[5]; // sum base^2
  double ss0[5]; // sum of 1/s^2

// separated X and Y resolutions added 2023.01.24
  double sigBPCD[2][4];
  double w00D[2][4];
  double ss1D[2][5]; // average Z   i=0, 1, 2, 3 fit without BPC#i 
               //             i=4          fit all 4 BPCs
  double ss2D[2][5]; // average Z^2
  double ssCD[2][5]; // sum (Z-<Z>)^2*W
  double ssBD[2][5]; // sum base^2
  double ss0D[2][5]; // sum of 1/s^2

  int spill_prev;

  double t_DIG_BPC_WAL_start; // first in spill
  double t_DIG_BPC_WAL_prev;  // from previous event
 
  double t_BPC_start; // first in spill
  double t_BPC_prev;  // from previous event
  double t_BPCsec_prev;  // from previous event

  double t_DIG_start; // first in spill
  double t_DIG_prev;  // from previous event

};

#endif
