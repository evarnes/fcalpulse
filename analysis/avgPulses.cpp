#include "avgPulseFinder.h"
#include "TChain.h"
#include "TH1F.h"
#include "TFile.h"

#define nHV_vals 17

TH1F* getAvgPulse(Int_t HV, Bool_t Sr90in) {
  avgPulseFinder apf(HV, Sr90in); 

  apf.Loop();

  TH1F* hout = (TH1F*)apf.Get_h_mid()->Clone();

  return hout;
}


int main() {

  int HV_vals[nHV_vals] = {300, 250, 200, 150, 130, 120, 110, 100, 90, 80, 70, 60, 50, 40, 30, 20, 10};

  TH1F *h_mid_p_in[nHV_vals], *h_mid_m_in[nHV_vals], *h_mid_d_in[nHV_vals];
  TH1F *h_mid_p_out[nHV_vals], *h_mid_m_out[nHV_vals], *h_mid_d_out[nHV_vals];

  char hname[100];

  TFile fout("avgPulses_SCal14.root", "RECREATE");

  for (int iHV = 0; iHV < nHV_vals; iHV++) {

    h_mid_p_in[iHV] = getAvgPulse(HV_vals[iHV], true);
    sprintf(hname, "FCal_mid_p%d_Sr90in", HV_vals[iHV]);
    h_mid_p_in[iHV]->SetName(hname);

    h_mid_p_out[iHV] = getAvgPulse(HV_vals[iHV], false);
    sprintf(hname, "FCal_mid_p%d_Sr90out", HV_vals[iHV]);
    h_mid_p_out[iHV]->SetName(hname);

    h_mid_m_in[iHV] = getAvgPulse(-HV_vals[iHV], true);
    sprintf(hname, "FCal_mid_m%d_Sr90in", HV_vals[iHV]);
    h_mid_m_in[iHV]->SetName(hname);
    
    h_mid_m_out[iHV] = getAvgPulse(-HV_vals[iHV], false);
    sprintf(hname, "FCal_mid_m%d_Sr90out", HV_vals[iHV]);
    h_mid_p_out[iHV]->SetName(hname);

    h_mid_d_in[iHV] = (TH1F*)h_mid_p_in[iHV]->Clone();
    sprintf(hname, "FCal_mid_d%d_Sr90in", HV_vals[iHV]);
    h_mid_d_in[iHV]->Add(h_mid_p_in[iHV], h_mid_m_in[iHV], 1, -1);
    h_mid_d_in[iHV]->SetName(hname);

    h_mid_d_out[iHV] = (TH1F*)h_mid_p_out[iHV]->Clone();
    sprintf(hname, "FCal_mid_d%d_Sr90out", HV_vals[iHV]);
    h_mid_d_out[iHV]->Add(h_mid_p_out[iHV], h_mid_m_out[iHV], 1, -1);
    h_mid_d_out[iHV]->SetName(hname);

    fout.cd();
    h_mid_p_in[iHV]->Write();
    h_mid_p_out[iHV]->Write();
    h_mid_m_in[iHV]->Write();
    h_mid_m_out[iHV]->Write();
    h_mid_d_in[iHV]->Write();
    h_mid_d_out[iHV]->Write();
  }

}
  

