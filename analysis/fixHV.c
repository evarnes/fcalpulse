// function to fix HV values that were not read in correctly
void fixHV(
	   UInt_t &runNumber,
	   Float_t         &FCal_inner_HV_set,
	   Float_t         &FCal_tubeB_HV_read,
	   Float_t         &FCal_tubeC_HV_read
	   )

{

  // only do swaps for run numbers that had a problem
  if ((runNumber > 999 && runNumber < 1065)   // FCP_DAQ v2.0.0 -2.1.1
      || (runNumber> 1110 && runNumber < 1126) ) { // FCP_DAQ v2.1.8 - 2.1.9
    // remap!
    FCal_tubeB_HV_read = FCal_inner_HV_set;
    FCal_inner_HV_set = FCal_tubeC_HV_read;
    FCal_tubeC_HV_read = 0.;
  }

}

