#ifndef BPC_Track_h
#define BPC_Track_h

#include <TLine.h>
#include <math.h>
#include "BPC_utils.h"
 
class BPC_Track {
private:
// position of everything along Z-axis
//#include "includes/Geometry2022.inc.C"  
//====================================
  BPC_utils BPCu;
  double x[4];     // position, mm
  double res[4];   // residual, mm
  double fit[4];   // fit result, mm
  double check[4]; // checksum 
  int    ihit[4];  // hit# in chamber
  int    BPC[4];   // BPC# for track point
  int    missingBPC; // chamber without a hit (or unused one)
  bool   BPCset[4];
  double x0;
  double angle;
  double x0E;
  double angleE;
  double chi2;
  
  bool fitDone;
  int index;
  int axis;
  int nPoints;
  
  TLine* myLine;
  
 public:
  BPC_Track();
  void Clear();
  void SetIndex(int indx, int ix);
  void Set( int indx,    int ix, 
	   double x0, double x1, double x2, double x3,
	   double s0, double s1, double s2, double s3);
  void Set( int indx,    int ix,
	    int   ih0, int   ih1, int   ih2, int   ih3,   
	    double x0, double x1, double x2, double x3,
	    double s0, double s1, double s2, double s3);
  void AddPoint(int iBPC, int ih, double xx, double cc);
  int GetBPChitIndex(int iBPC) { return ihit[iBPC];}
  int GetBPChitPosition(int iBPC) { return x[iBPC];}
  int GetBPChitCheckSum(int iBPC) { return check[iBPC];}
  int GetNpoints() { return nPoints;}
  int GetAxis() {  return axis;}
  void Fit();
  bool   CheckBPC(int i)    {return BPCset[i];}
  int    GetMissingBPC()    {return missingBPC;}
  double GetChi2()          {return chi2;}
  double GetS4()            {return angle*(BPCu.Z_S4-BPCu.ss1[missingBPC])+x0;}
  double GetCryo()          {return angle*(BPCu.Z0-BPCu.ss1[missingBPC])+x0;}
  double GetResidual(int i) {return res[i];}
  double GetFit(int i)      {return fit[i];}
  double GetAtPlane(int i)  {return angle*(BPCu.BPC_Z[i]-BPCu.ss1[missingBPC])+x0;}
  double GetAtZ(double z)   {return angle*(z-BPCu.ss1[missingBPC])+x0;}
  double GetAngle()         {return angle;}
  double GetAngleError()    {return 1.0/sqrt(BPCu.ssC[missingBPC]);}
  double GetErrAtZ(double z); 
  void Print();
};

#endif
