#ifndef pulseFractionWriter_h
#define pulseFractionWriter_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <iostream>
#include <fstream>
#include "FCPAnalysisBase.h"
#include "BPC_Tracker.h"

class pulseFractionWriter : public FCPAnalysisBase {

  using FCPAnalysisBase::FCPAnalysisBase;

public :
  
  pulseFractionWriter(Int_t HV_in, bool Sr90in);

  // functions that implement the actual analysis in this case
  virtual void Init(TTree* t);
  virtual Int_t  Execute( );
  virtual void Finalize();

 private:

  BPC_Tracker BPCt;
  Int_t HV;
  unsigned nBins;
  unsigned startBin;
  unsigned endBin;
  unsigned nPedBins;
  std::ofstream f_frac;
  std::ofstream f_HV;
  std::ofstream f_r;
  std::ofstream f_z;
  Int_t nSuccess[8];
  Int_t nFail[8];
  Float_t HV_r_1;
  Int_t nPassTrig;
  Int_t nPassPulseTot;
  std::vector<Double_t> pulseFracAvg;
  std::vector<Double_t> pulseFracStdDev;
  unsigned prevRun;
  unsigned nSamples;
  char rootFileName[200];
  TH1F* h_pulseTot;

};

#endif
