#!/usr/bin/python3

# script to print electronic log information from a single run 
# (and optionally spill)

import os

logdir = "/data1/tb2022/data/2022/logfiles"

goodInput = False

while not goodInput :
    runNum = input("Enter the run number: ")
    try:
        runNum = int(runNum) 
        goodInput = True
    except:
        print("Sorry, that's not a valid number.  Please try again")

goodInput = False
while not goodInput :
    spillNum = input("Enter the spill number (negative for all spills): ")
    try:
        spillNum = int(spillNum)
        goodInput = True
    except:
        print("Sorry, that's not a valid number.  Please try again")

#get the name of the correct log file
cmd = "grep 'run "+str(runNum)+"' "+logdir+"/* >> doi.txt"
os.system(cmd)
 
fdoi = open("doi.txt")
for l in fdoi :
    l1 = l.split("/")
    logname = l1[len(l1)-1]
    l2 = logname.split("log")
    logname = logdir+"/"+l2[0]+"log"
    break

try: 
    flog = open(logname)
except:
    print("Couldn't open", logname)

printThisLine = False
inCorrectRun = False

for l in flog:    
    if (("Begin run "+str(runNum)) in l) :
        inCorrectRun = True
        if (spillNum < 0) :
            printThisLine = True
    if (("Begin run "+str(runNum+1)) in l) :
        break
     
    if (inCorrectRun and ("read beam: spill" in l)) :
        l2 = l.split()
        if (l2[3] == str(spillNum)) :
            printThisLine = True

    if (printThisLine) :
        print(l, end="")

    if (printThisLine and (spillNum > 0) ) :
        if (inCorrectRun and ("read beam: spill" in l)) :
            l2 = l.split()
            if (l2[3] == str(spillNum+1)) :
                break
