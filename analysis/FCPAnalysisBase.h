//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Sat Aug 13 05:23:08 2022 by ROOT version 6.14/06
// from TTree merged_digitizer_BPC/merged_digitizer_BPC
// found on file: run733.root
//////////////////////////////////////////////////////////

#ifndef FCPAnalysisBase_h
#define FCPAnalysisBase_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <iostream>
#include <fstream>
#include "Digitizer_leaves.h"
#include "Dig_utils.h"
#include "BPC_utils.h"

class FCPAnalysisBase {
 public :
  Digitizer_leaves *digL;

  FCPAnalysisBase(TTree *tree=0);
  virtual Int_t GetEntry(Long64_t entry);
  virtual Long64_t LoadTree(Long64_t entry);


  void     Loop();

 // each analysis must implement its own Execute() function
 // Init() and Finalize() are optional
  virtual Int_t Execute() = 0;
  virtual void Init(TTree *tree);
  virtual void Finalize() {;}

 protected:
  Dig_utils Digu;
  BPC_utils BPCu;
  Long64_t jentry;
  Long64_t ientry;
  Long64_t nentries;
};

#endif


