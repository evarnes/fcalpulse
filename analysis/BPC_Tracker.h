#ifndef BPC_Tracker_h
#define BPC_Tracker_h

#include <TLine.h>
#include <TArc.h>
#include <TLatex.h>
#include <TProfile.h>
#include <TH2F.h>
#include <TCanvas.h>
#include <TFile.h>
#include <fstream>
#include "BPC_Track.h"                // track object
#include "BPC_utils.h"
#include "BPC_leaves.h"

class BPC_Tracker {
//-------------------------------
private:

  BPC_leaves bpcL;
  int pattern_prev; 
  int calibEvents;
  int nEventsBPC;                // number of evvents in BPC run
  int bpcEntry;                  // current BPC Entry
  const int TRG=4; // beam trigger pattern
  const int CAL=2; // calibration pattern
  const int nBPCmin=3; // no tracks fit with less than 3 BPCs

  //-------------------> BPC calibration tics->mm
  double calibBPC0[2][4][3]={24*0.0}; //wire position, TDC units
  double calibBPC1[2][4][3]={24*0.0};
  double calibBPC2[2][4][3]={24*0.0};
  
  
  double calibBPC0c[2][4][3]={24*0.0}; // index#3 0 wires  7 and 16 slopes
  double calibBPC1c[2][4][3]={24*0.0}; //         1 wires  7 and 25
  double calibBPC2c[2][4][3]={24*0.0}; //         2 wires 16 and 25
  
  double calibBPCfit[2][4][3]={24*0.0}; // gauss fit of the calibration data
  double calibBPCerr[2][4][3]={24*0.0};
  double calibBPCsig[2][4][3]={24*0.0};
  double calibBPCser[2][4][3]={24*0.0};

//-------------------> BPC alignment realtive to S4
  double alignmentS4[4][2];
  double alignmentTrack5[4][2][3][8]; // BPC# axis# fit# peak errP sigma errS mean errM rms errR 
  //-------------------> Tube alignment relative to tracker
  double xTube[4];
  double xTubeE[4];
  double yTube[4];
  double yTubeE[4];
  double TubePosition[2][4];
  double dummy;
  
  double zStart;
  double zEnd;

//-------------------------------------------------.
  int tiksCount[2][4][2];                  // number of tiks (X/Y, BPC, side)
  std::vector<double> bpc_hits[2][4];      // hits in bpc_hits[0][i]->x 
  //         bpc_hits[1][i]->y
  std::vector<int>    bpc_at16indx[2][4];  // index at bpc_hits vector;
  std::vector<double> bpc_at16[2][4];      // hits, calibrated in mm
  // used in CheckSum analysis       
  std::vector<double> bpc_at16cor0[2][4];  // hits, calibrated in mm
  // X-coordinate in BPC#0, 2, 3 reversed!
  std::vector<double> bpc_at16cor1[2][4];  // hits, calibrated in mm
  // X-coordinate in BPC#0, 2, 3 reversed!
  // aligned to S4 and corrected for "step5"
  // residuals startig from "step6"
  //std::vector<int>    bpc_at16points[2][4];// points this hit is included;
  std::vector<bool>   bpc_at16fit[2][4];   // true means use it for tracking;
  // hit is ether part of a "good point" or
  // has no pair in other plane of same BPC;
  std::vector<double> checkSum[2][4];      // 
  std::vector<int>    bpc_tikI[2][4][2];   // tik index used to form hit (X/Y, BPC, side)
  std::vector<int>    bpc_tikT[2][4][2];   // tik TDC used to form hit
  std::vector<double> point_16_Qval[4];    // Check Sum "quality" of a point - sum of "quality" X-checkY 
  //                                               and Y-checkX
  std::vector<double> point_16_Qsum[4];    // Check Sum chi2 style
  std::vector<int>    point_16_hitI[2][4]; // indexes of hit x and hit y of Point
  std::vector<int>    point_16_Order[4];   // ordered by Qsum, High to low;
  
  BPC_Track myTrack; 
  std::vector<BPC_Track> Tracks[2];            // X and Y projection tracks
  std::vector<int>   TracksOrder[2];       // ordered in chi2 low->high
  int nTracks[2];
  int nTracksGood[2];
  bool TrackingFailed[10]; // 0 - badBPC flag                           fatal
  // 1 - Nhits<3 in X or Y                     fatal
  // 2 - Ntracks X or Y is zero                fatal
  // 3 - tracking spread >0.5mm                fatal
  // 4 - angule >0.5mRad                       fatal
  // 5 - R>5mm at S4;                          optional
  // 6 - track envlope failed                  fatal
  // 7 - distance to tube sum center > 4mm     optional
  
  double posImpact[3][2];
  double angImpact[3][2];
  double Rimpact[4]; // 3 tubes and sum
  double Aimpact[4]; // 3 tubes and sum
  
  double posS4[3][2];
  bool   failedAccuracy;
  bool   good_BPC_data;
  double posImpact2sum;
  
  
  std::string calib_dir;
  std::string data_dir;
  
  std::string cAxis;   // X or Y
  std::string cWide;  // Wide or Narrow 
  char cEventBPC[200];
  char cEventDIG[200];
  Int_t cCol;
  
  char version_in1[200];
  char version_in3[200];
  char version_in5[200];
  char version_in_fit[200];
  char version_env[200];
  
  char myText[1000];
  char myTex0[200];
  char fname[1000];
  char hname[200];
  
  std::ifstream fileIN;
  
  bool showIt=false;
  int isWide;  // wide=1 or narrow=0 beam - for use with beam envelope
  int myRun;   // run to analyse
  
  double BPCdispRange;
  
  TFile   *fBPC; // Root file with bpc run data 
  TTree   *bpcD; // BPC tree
  
  TProfile* EnvelopesPRacc[2][2]; // envelope profiles created by "summary_step6fast.C"
  //======================== checksum vs Other =================================
  std::vector<double>xCheck[2][4];   // x-point for checSum cut [axis]][plane]
  std::vector<double>sCheck[2][4];   // mean for checSum cut [axis]][plane]
  std::vector<double>rCheck[2][4];   // rms for checSum cut [axis]][plane]
  std::vector<double>sCheckF[2][4];  // fit peak for checSum cut [axis]][plane]
  std::vector<double>rCheckF[2][4];  // fit sigma for checSum cut [axis]][plane]
  int    ixd, ipd, npd;
  double xcm, ycm, rcm, ycf, rcf, dxy6;
  
  
  TCanvas *trackingCanvas; // BPC display canvas
  TH2F    *trackingHits;   // hits per plane;
  TH2F    *trackingTracks; // tracks projections
  TLine   *myLine;
  TArc    *myArc;
  TLatex  *myLatex;
  
  int cSize=600;
  
  BPC_utils BPCu;


//====================================================================
public:
  BPC_Tracker();

//====================================================================

  void Init();

//
// brings x to range [-range/2. range/2.]
//
 double BringToRange(double x, double range);


//====================================================================
 void displayTracker(const int spillDIG, const int digEntry);

//====================================================================
 bool InitRun(const int r, const int isW);

//-------------------------------
 bool ReadBPCevent(const int b);

 bool ReadTRIGGERevent();

//-------------------------------
 bool doTracking(const bool good);

//-------------------------------
 int    GetBPCentry() {return bpcEntry;}
 int    GettBPC()     {return bpcL.tBPC;}
 double GettBPCsec()  {return bpcL.tBPCsec;}
 int    GetRun()      {return myRun;} 
 //-------------------------------
 void CloseRun()    {fBPC->Close();}
 //-------------------------------
 // check if criteria #i failed
 bool GetFailed(const int i) {return TrackingFailed[i];}
 //-------------------------------
 // position X, Y at front of cryostat
 double GetPosImpact(const int ix) {
   return posImpact[1][ix];
 }
//-------------------------------
// position X, Y at front of cryostat
 double GetPosRMS(const int ix) {
  return posImpact[2][ix];
 }
//-------------------------------
// track angle
 double GetAngImpact(const int ix) {
  return angImpact[1][ix];
 }
//-------------------------------
// tracks angle spread
 double GetAngRMS(const int ix) {
  return angImpact[2][ix];
 }
//-------------------------------
 double GetS4Impact(const int ix) {
// position relative to S4 center
  return posS4[1][ix];
 }
//-------------------------------
// distance to a tube senter
 double GetRimpact(const int it) { 
  return Rimpact[it];
 }
//-------------------------------
// asimuth around a tube senter
 double GetAimpact(const int it) { 
  return Aimpact[it];
 }

//-------------------------------
// get tube position vs BPC
 double GetTubePosition(const int ix, const int it) {
  return TubePosition[ix][it];
 }

//------------------------------
// get tracker display for saving;
 TCanvas* GetCanvas() {return trackingCanvas;}


};

#endif
