// only do swaps for run numbers that had a problem
if ((myRun > 999  && myRun < 1065)   // FCP_DAQ v2.0.0 -2.1.1
 || (myRun > 1110 && myRun < 1126) ) { // FCP_DAQ v2.1.8 - 2.1.9
// remap!
  FCal_tubeB_HV_read = FCal_inner_HV_set;
  FCal_inner_HV_set  = FCal_tubeC_HV_read;
  FCal_tubeC_HV_read = 0.;
}
