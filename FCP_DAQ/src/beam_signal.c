/* Program to simulatate the beam on/off signal */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <signal.h>
#include "portnumber.h"
#include "dispatch.h"

#define BEAM_ON_SEC 4
#define BEAM_OFF_SEC 21

#define MaxConnects 1

static long get_time()
{
    long time_ms;
#ifdef WIN32
    struct _timeb timebuffer;
    _ftime( &timebuffer );
    time_ms = (long)timebuffer.time * 1000 + (long)timebuffer.millitm;
#else
    struct timeval t1;
    //    struct timezone tz;
    gettimeofday(&t1, 0);
    time_ms = (t1.tv_sec) * 1000 + t1.tv_usec / 1000;
#endif
    return time_ms;
}

int main() {

//Connect to control host
  init_disp_link(NULL, "");
  my_id("beam_signal");

  sigaction(SIGPIPE, &(struct sigaction){{SIG_IGN}}, NULL);

  // set up socket and start listening
  int fd = socket(AF_INET,     /* network versus AF_LOCAL */
                  SOCK_STREAM, /* reliable, bidirectional, arbitrary payload size */
                  0);          /* system picks underlying protocol (TCP) */
  if (fd < 0) {
    printf("Unable to open socket\n");
    exit(-1);
  }
  
  //  int optval = 1;
  //int optlen = sizeof(optval);
  // setsockopt(fd, SOL_SOCKET, SO_KEEPALIVE, &optval, optlen);

  /* bind the server's local address in memory */
  struct sockaddr_in saddr;
  memset(&saddr, 0, sizeof(saddr));          /* clear the bytes */
  saddr.sin_family = AF_INET;                /* versus AF_LOCAL */
  saddr.sin_addr.s_addr = htonl(INADDR_ANY); /* host-to-network endian */
  saddr.sin_port = htons(PortNumber);        /* for listening */
  
  if (bind(fd, (struct sockaddr *) &saddr, sizeof(saddr)) < 0) {
    printf("Unable to bind socket\n");
    exit(-1);
  }
  
  while (1) {
    /* listen to the socket */
    if (listen(fd, MaxConnects) < 0) { /* listen for clients, up to MaxConnects */
      printf("Error listening to socket\n");
      exit(-1);
    }
    
    printf("Listening on port %i for clients...\n", PortNumber);
    
    // start with beam on
    int beam_on = 1;
    
    struct sockaddr_in caddr; /* client address */
    unsigned len = sizeof(caddr);  /* address length could change */
    
    int client_fd = accept(fd, (struct sockaddr*) &caddr, &len);  /* accept blocks */
    if (client_fd < 0) {
      printf("Problem with accept\n"); /* don't terminate, though there's a problem */
    } 
    
    char buffer[20];
    memset(buffer, '\0', sizeof(buffer));
    
    char rdbuffer[100000];
    int good_connect = 1;
    uint64_t LastChangeTime = get_time();
    int in_run = 0;

    while (good_connect) {
      
      uint64_t CurrentTime = get_time();
      uint64_t ElapsedTime = CurrentTime -  LastChangeTime;

      //      printf("client_fd = %d\n", client_fd);
      if (read(client_fd, rdbuffer, sizeof(rdbuffer)) < 0) {
      	printf("Error reading from socket\n");
      } else {
	if (strstr(rdbuffer, "start run") != NULL) {
	  printf("Saw start run\n");
	  in_run = 1;
	  // reset spill numbers to 1
	  FILE* spillNumFile = fopen("spillNumber.txt", "r+");
	  if (spillNumFile == NULL) {
	    printf("Error: can't open spillNumber.txt file\n");
	    exit(-1);
	  }
	  char spillNumStr[20];
	  sprintf(spillNumStr, "%d\n", 1);
	  rewind(spillNumFile);
	  fputs(spillNumStr, spillNumFile);
	  fclose(spillNumFile);

	}
	if (strstr(rdbuffer, "resume run") != NULL) {
	  in_run = 1;
	}
	if (strstr(rdbuffer, "stop run") != NULL) {
	  in_run = 0;
	}	
      }

      //      printf("ElapsedTime = %d\n", ElapsedTime);
      if ((beam_on == 1) && (ElapsedTime >= BEAM_ON_SEC*1000)) {
	beam_on = 0;
	sprintf(buffer, "beam off");
	if (write(client_fd, buffer, sizeof(buffer)) < 0) {
	  good_connect = 0;
	}
	printf("Beam off\n");
	LastChangeTime = CurrentTime;
	ElapsedTime = 0;
	//send to ControlHost
	put_fullstring("CMD", "EoS");
      }
      if ((beam_on == 0) && (ElapsedTime >= BEAM_OFF_SEC*1000)) {
	beam_on = 1;
	printf("Beam on\n");
	if (in_run == 1) {
	  FILE* spillNumFile = fopen("spillNumber.txt", "r+");
	  if (spillNumFile == NULL) {
	    printf("Error: can't open spillNumber.txt file\n");
	    exit(-1);
	  }
	  char spillNumStr[20];
	  fgets(spillNumStr, 20, spillNumFile);
	  int spillNum = atoi(spillNumStr);
	  sprintf(buffer, "beam spill %i", spillNum);
	  if (write(client_fd, buffer, sizeof(buffer)) < 0) {
	    good_connect = 0;
	  }
	  sprintf(spillNumStr, "%d\n", spillNum+1);
	  rewind(spillNumFile);
	  fputs(spillNumStr, spillNumFile);
	  fclose(spillNumFile);
	} else {
	  sprintf(buffer, "beam spill %i", -1);
	  if (write(client_fd, buffer, sizeof(buffer)) < 0) {
	    good_connect = 0;
	  }
	}
	LastChangeTime = CurrentTime;
	ElapsedTime = 0;
	//send to ControlHost
	put_fullstring("CMD", "SoS");

      }
    }
  }
}
