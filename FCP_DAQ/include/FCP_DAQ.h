/******************************************************************************

******************************************************************************/

#ifndef _FCP_DAQ_H_
#define _FCP_DAQ_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <pthread.h>
//#include <fstream.h>
#include <CAENDigitizer.h>
#include <CAENDigitizerType.h>
#include <gtk/gtk.h>

#define NUM_ADC_BOARDS 4  // number of digitizers to read out
#define NUM_BPCS 4  // number of beam position counters

#include <unistd.h>
#include <stdint.h>   /* C99 compliant compilers: uint64_t */
#include <ctype.h>    /* toupper() */
#include <sys/time.h>

#include <vector>
#include "TDatime.h"

#define		_PACKED_		__attribute__ ((packed, aligned(1)))
#define		_INLINE_		__inline__ 

#define NUM_ADC_CHANNELS 8           /* max. number of independent settings */

#define NUM_GW  20        /* number of generic write commands */


#define NHV_set 3   /* number of HV supplies */
#define NHV_read 4
#define NCRYOSTATS 3  /* number of cryostats */

#define MAX_ERRORS 100 /* maximum number of errors that can be tracked */

/* ###########################################################################
   Typedefs
   ###########################################################################
*/

typedef struct {
  CAEN_DGTZ_ConnectionType  LinkType;
  int LinkNum;
  int ConetNode;
  int Nch;
  int Nbit;
  float Ts;
  int NumEvents;
  uint32_t RecordLength;
  uint32_t PostTrigger;
  int TestPattern;
  CAEN_DGTZ_IOLevel_t FPIOtype;
  CAEN_DGTZ_TriggerMode_t ExtTriggerMode[NUM_ADC_BOARDS];
  uint16_t EnableMask[NUM_ADC_BOARDS];
  CAEN_DGTZ_TriggerMode_t ChannelTriggerMode[NUM_ADC_BOARDS][NUM_ADC_CHANNELS];
  CAEN_DGTZ_PulsePolarity_t PulsePolarity[NUM_ADC_BOARDS][NUM_ADC_CHANNELS];
  uint32_t DCoffset[NUM_ADC_BOARDS][NUM_ADC_CHANNELS];
  float_t Vrange[NUM_ADC_BOARDS][NUM_ADC_CHANNELS];
  int GWn;
  uint32_t GWaddr[NUM_GW];
  uint32_t GWdata[NUM_GW];
  uint32_t GWmask[NUM_GW];
  uint16_t scintUsed; // 0 = small, 1 = large
  uint16_t sourceInBeam; // 0 = false, 1 = true
  uint16_t includeTailVeto;
} FCP_DAQ_Config_t;

typedef struct {
  uint32_t id;
  char msg[200];
  GtkButton* ackButton;
  int isAcknowledged;
} FCP_DAQerror;

typedef struct {
  int InRun;
  int AcqRun;
  int PauseRun;
  int EndRun;
  int StartPushedDuringSpill;
  FILE *fout_allch;
  uint32_t spillNumber;
  uint32_t boardFailBits[NUM_ADC_BOARDS];
  uint32_t ackBoardFailBits[NUM_ADC_BOARDS];
  float HV_set[NHV_set];  // 0 = SCal, 1 = FCal inner, 2 = FCal outer
  float HV_read[NHV_read]; // 0 = SCal, 1 = FCal front tube, 2 = FCal middle B, 
                           // 3 = Fcal rear C 
  uint32_t BeamStatus;
  pthread_t AnalysisThread;
  uint16_t NewDataToAnalyze;
  uint16_t DataCopyInProgress;
  uint16_t nErrors;
  uint16_t autoPause;
  FCP_DAQerror errors[MAX_ERRORS];
  uint16_t fanSpeed[NUM_ADC_BOARDS];
} FCP_DAQ_Run_t;

typedef struct {
  int handle_ADC[NUM_ADC_BOARDS];
  int nTriggersInRun;
  FCP_DAQ_Run_t *FCP_DAQrun;
  FCP_DAQ_Config_t *FCP_DAQcfg;
  char beam_status[200];
  off_t MaxFileSize;
  CAEN_DGTZ_BoardInfo_t BoardInfo[NUM_ADC_BOARDS];
  guint DAQ_thread_ID;
  guint analysis_thread_ID;
  guint beam_status_thread_ID;
  char* buffer_ADC[NUM_ADC_BOARDS];
  char* analysisBuffer_ADC[NUM_ADC_BOARDS];
  uint32_t BufferSize_ADC[NUM_ADC_BOARDS];
  GtkWidget *window;
  GtkWidget *start;
  GtkWidget *pause;
  GtkWidget *stop;
  GtkWidget *exit;
  GtkWidget *scintChoice0;
  GtkWidget *scintChoice1;
  GtkWidget *ShCalPed0;
  GtkWidget *ShCalPed1;
  GtkWidget *assemblyInBeam0;
  GtkWidget *assemblyInBeam1;
  GtkWidget *includeTailVeto;
  GtkWidget *nevents_bar[NUM_ADC_BOARDS];
  GtkWidget *nevents_bar_label[NUM_ADC_BOARDS];
  GtkWidget *BPC_nevents_bar;
  GtkWidget *BPC_nevents_bar_label;
  GtkWidget *beam_status_label;
  GtkWidget *run_status_label;
  GtkWidget *assembly_in_beam_label;
  GtkWidget *disk_space_label;
  char       prevSocketMessage[20];
  CAEN_DGTZ_UINT16_EVENT_t    *Event16[NUM_ADC_BOARDS];
} FCP_DAQ_Info_t;

typedef struct {
  uint32_t runNumber;
  // software version used to write the data
  uint32_t dataFormatVersion;
  // block of digitizer information and configuration parameters
  uint32_t channelDataSize;
  uint32_t NumEvents;
  uint32_t RecordLength;
  uint32_t PostTrigger;
  uint32_t FPIOtype;
  uint32_t NumBoards;
  uint32_t SerialNumber[NUM_ADC_BOARDS];
  uint32_t ExtTriggerMode[NUM_ADC_BOARDS];
  uint32_t EnableMask[NUM_ADC_BOARDS]; 
  uint32_t DCoffset[NUM_ADC_BOARDS][NUM_ADC_CHANNELS];
  float_t  Vrange[NUM_ADC_BOARDS][NUM_ADC_CHANNELS];
  uint32_t ChannelTriggerMode[NUM_ADC_BOARDS][NUM_ADC_CHANNELS];
  uint32_t PulsePolarity[NUM_ADC_BOARDS][NUM_ADC_CHANNELS];
  uint32_t GWn;
  uint32_t GWaddr[NUM_GW];
  uint32_t GWdata[NUM_GW];
  uint32_t GWmask[NUM_GW];
  uint16_t scintUsed; // 0 = small, 1 = large
  uint16_t sourceInBeam; // 0 = false; 1 = true
  uint16_t includeTailVeto;
  uint16_t reserved_1;  // to keep word alignment
  // block of information about other hardware  
  uint32_t nHV_set;
  uint32_t nHV_read;
  uint32_t nCryostats;
  uint16_t FCal_rod_front_source_digitizer_SN;
  uint16_t FCal_rod_front_source_digitizer_handle;
  uint16_t FCal_rod_front_source_channel;
  uint16_t FCal_rod_mid_source_digitizer_SN;
  uint16_t FCal_rod_mid_source_digitizer_handle;
  uint16_t FCal_rod_mid_source_channel;
  uint16_t FCal_rod_rear_source_digitizer_SN;
  uint16_t FCal_rod_rear_source_digitizer_handle;
  uint16_t FCal_rod_rear_source_channel;
  uint16_t FCal_rod_front_nosource_digitizer_SN;
  uint16_t FCal_rod_front_nosource_digitizer_handle;
  uint16_t FCal_rod_front_nosource_channel;
  uint16_t FCal_rod_mid_nosource_digitizer_SN;
  uint16_t FCal_rod_mid_nosource_digitizer_handle;
  uint16_t FCal_rod_mid_nosource_channel;
  uint16_t FCal_rod_rear_nosource_digitizer_SN;
  uint16_t FCal_rod_rear_nosource_digitizer_handle;
  uint16_t FCal_rod_rear_nosource_channel;
  uint16_t ShCal_front_source_quad_1_digitizer_SN;
  uint16_t ShCal_front_source_quad_1_digitizer_handle;
  uint16_t ShCal_front_source_quad_1_channel;
  uint16_t ShCal_front_source_quad_2_digitizer_SN;
  uint16_t ShCal_front_source_quad_2_digitizer_handle;
  uint16_t ShCal_front_source_quad_2_channel;
  uint16_t ShCal_front_source_quad_3_digitizer_SN;
  uint16_t ShCal_front_source_quad_3_digitizer_handle;
  uint16_t ShCal_front_source_quad_3_channel;
  uint16_t ShCal_front_source_quad_4_digitizer_SN;
  uint16_t ShCal_front_source_quad_4_digitizer_handle;
  uint16_t ShCal_front_source_quad_4_channel;
  uint16_t ShCal_rear_source_quad_1_digitizer_SN;
  uint16_t ShCal_rear_source_quad_1_digitizer_handle;
  uint16_t ShCal_rear_source_quad_1_channel;
  uint16_t ShCal_rear_source_quad_2_digitizer_SN;
  uint16_t ShCal_rear_source_quad_2_digitizer_handle;
  uint16_t ShCal_rear_source_quad_2_channel;
  uint16_t ShCal_rear_source_quad_3_digitizer_SN;
  uint16_t ShCal_rear_source_quad_3_digitizer_handle;
  uint16_t ShCal_rear_source_quad_3_channel;
  uint16_t ShCal_rear_source_quad_4_digitizer_SN;
  uint16_t ShCal_rear_source_quad_4_digitizer_handle;
  uint16_t ShCal_rear_source_quad_4_channel;
  uint16_t ShCal_front_nosource_quad_1_digitizer_SN;
  uint16_t ShCal_front_nosource_quad_1_digitizer_handle;
  uint16_t ShCal_front_nosource_quad_1_channel;
  uint16_t ShCal_front_nosource_quad_2_digitizer_SN;
  uint16_t ShCal_front_nosource_quad_2_digitizer_handle;
  uint16_t ShCal_front_nosource_quad_2_channel;
  uint16_t ShCal_front_nosource_quad_3_digitizer_SN;
  uint16_t ShCal_front_nosource_quad_3_digitizer_handle;
  uint16_t ShCal_front_nosource_quad_3_channel;
  uint16_t ShCal_front_nosource_quad_4_digitizer_SN;
  uint16_t ShCal_front_nosource_quad_4_digitizer_handle;
  uint16_t ShCal_front_nosource_quad_4_channel;
  uint16_t ShCal_rear_nosource_quad_1_digitizer_SN;
  uint16_t ShCal_rear_nosource_quad_1_digitizer_handle;
  uint16_t ShCal_rear_nosource_quad_1_channel;
  uint16_t ShCal_rear_nosource_quad_2_digitizer_SN;
  uint16_t ShCal_rear_nosource_quad_2_digitizer_handle;
  uint16_t ShCal_rear_nosource_quad_2_channel;
  uint16_t ShCal_rear_nosource_quad_3_digitizer_SN;
  uint16_t ShCal_rear_nosource_quad_3_digitizer_handle;
  uint16_t ShCal_rear_nosource_quad_3_channel;
  uint16_t ShCal_rear_nosource_quad_4_digitizer_SN;
  uint16_t ShCal_rear_nosource_quad_4_digitizer_handle;
  uint16_t ShCal_rear_nosource_quad_4_channel;
  uint16_t S1_counter_digitizer_SN;
  uint16_t S1_counter_digitizer_handle;
  uint16_t S1_counter_channel;
  uint16_t S2_counter_digitizer_SN;
  uint16_t S2_counter_digitizer_handle;
  uint16_t S2_counter_channel;
  uint16_t S3_counter_digitizer_SN;
  uint16_t S3_counter_digitizer_handle;
  uint16_t S3_counter_channel;
  uint16_t S4_counter_digitizer_SN;
  uint16_t S4_counter_digitizer_handle;
  uint16_t S4_counter_channel;
  uint16_t Halo_counter_digitizer_SN;
  uint16_t Halo_counter_digitizer_handle;
  uint16_t Halo_counter_channel;
  uint16_t Leakage_counter_digitizer_SN;
  uint16_t Leakage_counter_digitizer_handle;
  uint16_t Leakage_counter_channel;
  uint16_t Digitizer_1_trigger_digitizer_SN;
  uint16_t Digitizer_1_trigger_digitizer_handle;
  uint16_t Digitizer_1_trigger_channel;
  uint16_t Digitizer_2_trigger_digitizer_SN;
  uint16_t Digitizer_2_trigger_digitizer_handle;
  uint16_t Digitizer_2_trigger_channel;
  uint16_t Digitizer_3_trigger_digitizer_SN;
  uint16_t Digitizer_3_trigger_digitizer_handle;
  uint16_t Digitizer_3_trigger_channel;
  uint16_t Digitizer_4_trigger_digitizer_SN;
  uint16_t Digitizer_4_trigger_digitizer_handle;
  uint16_t Digitizer_4_trigger_channel;
} FCP_DAQ_RunHeader_t;

typedef struct {
  time_t spillStartTime;
  uint32_t spillNumber;
  uint32_t boardFailBits[NUM_ADC_BOARDS];
  float HV_set[NHV_set];
  float HV_read[NHV_read];
  uint32_t BeamStatus;
  uint32_t NumEvents;
  uint32_t reserved; // for word alignment
} FCP_DAQ_SpillHeader_t;

typedef struct {
  uint32_t nchannels;
  uint32_t eventCounter;
  uint32_t triggerTimeTag[NUM_ADC_BOARDS];
} FCP_DAQ_EventHeader_t;

typedef struct {
  uint32_t channel;
  uint32_t eventCounter;    // these two fields are duplicated from eventheader
  //  uint32_t triggerTimeTag;  // for now.  Should probably fix...
} FCP_DAQ_ChannelHeader_t;

typedef struct {
  uint16_t tag; // 0xCAFE for event header, 0xFACE for SoS, 0xECAF for EoS
  uint16_t runNumber;
  uint16_t spillNumber;
  uint16_t triggerNumber;
  uint32_t accum_time;
  uint32_t wall_time;
  float xy[NUM_BPCS][2];  // entry 0 is x, entry 1 is y
} BPC_Data;


//***************************************************************************
// Global variables for storing data to be written to root trees
//***************************************************************************
FCP_DAQ_RunHeader_t* g_runHeader;
FCP_DAQ_SpillHeader_t* g_spillHeader;
FCP_DAQ_EventHeader_t* g_eventHeader;
FCP_DAQ_ChannelHeader_t* g_channelHeader;
FCP_DAQ_Config_t* g_config;

BPC_Data g_BPC_data_sample;

std::vector<std::vector<uint16_t> > g_ADC_data;

TDatime g_comment_time;
char  g_comment_string[500];
#endif
