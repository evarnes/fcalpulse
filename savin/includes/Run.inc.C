struct Run {
private:
  int number;
  std::vector<int> spill_list;
public:
void  SetNumber(int run) {
  number=run;
  spill_list.clear();
}
void  AddSpill(int spill)   {spill_list.push_back(spill);}

void  OrderSpills() {
  int moved=1;
  while (moved>0) {
    moved=0;
    for (int i=0; i<(int)spill_list.size()-1; i++) {
      int j;
      if (spill_list.at(i) > spill_list.at(i+1)) {
        j=spill_list.at(i);
        spill_list.at(i)=spill_list.at(i+1);
        spill_list.at(i+1)=j;
        moved++;
      }
    }
  }
}

int   GetNumber()           {return number;}
int   GetNumberOfSpills()   {return (int)spill_list.size();}
int   GetSpillNumber(int i) {
  if (i<0 || i>=(int)spill_list.size()) {
    return -1;
  } else {
    return spill_list.at(i);
  }
}
void Print() {
  printf("Run#%d, %d spills ----->\n", number, this->GetNumberOfSpills());
  for (int i=0; i<(int)spill_list.size(); i++) {
    printf("\t%04d ", spill_list.at(i));
    if (i%10==0 && i>0) printf("\n");
  }
  printf("\n");
}
void Print(int iopt) {
  printf("Run#%d, %d spills ", number, this->GetNumberOfSpills());
  if (iopt>0) {
    printf("----->\n");
    for (int i=0; i<(int)spill_list.size(); i++) {
      printf("\t%04d ", spill_list.at(i));
      if (i%10==0 && i>0) printf("\n");
    }
  }
  printf("\n");
}
}; 


