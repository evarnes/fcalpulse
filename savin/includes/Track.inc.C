struct Track {
private:
// position of everything along Z-axis
//#include "includes/Geometry2022.inc.C" 
#include "GeometryNew.inc.C" 
//====================================
double x[4];     // position, mm
double res[4];   // residual, mm
double fit[4];   // fit result, mm
double check[4]; // checksum 
int    ihit[4];  // hit# in chamber
int    BPC[4];   // BPC# for track point
int    missingBPC; // chamber without a hit (or unused one)
bool   BPCset[4];
double x0;
double angle;
double x0E;
double angleE;
double chi2;

bool fitDone;
int index;
int axis;
int nPoints;

TLine* myLine;

public:
Track() {
#include "PrepareGeometry.inc.C"
  myLine=new TLine();
  index=0;
  axis=0;
  fitDone=false;
  x0=0.;
  angle=0.;
  x0E=0.;
  angleE=0.;
  chi2=0.;
  for (int i=0; i<4; i++) {
    x[i]=0.0;
    res[i]=0.0;
    BPCset[i]=false;
  }
  nPoints=0;
}
//-------------------------------------------------------
void Clear() {
  nPoints=0;
  fitDone=false;
  for (int i=0; i<4; i++) {
    x[i]=-10000.;
    check[i]=-10000.;
    ihit[i]=-1;
    BPC[i]=-1;
    BPCset[i]=false;
  }
}
//-------------------------------------------------------
void SetIndex(int indx, int ix) {
  index=indx;
  axis=ix;
}
//-------------------------------------------------------
void Set( int indx,    int ix, 
         double x0, double x1, double x2, double x3,
         double s0, double s1, double s2, double s3) {
  fitDone=false;
  x[0]=x0;
  x[1]=x1;
  x[2]=x2;
  x[3]=x3;
  check[0]=s0;
  check[1]=s1;
  check[2]=s2;
  check[3]=s3;
  nPoints=4;
  for (int i=0; i<4; i++) {
    BPC[i]=i;
    BPCset[i]=true;    
  }
}
//-------------------------------------------------------
void Set( int indx,    int ix,
         int   ih0, int   ih1, int   ih2, int   ih3,   
         double x0, double x1, double x2, double x3,
         double s0, double s1, double s2, double s3) {
  fitDone=false;
  index   =indx;
  axis    =ix;
  x[0]    =x0;
  x[1]    =x1;
  x[2]    =x2;
  x[3]    =x3;
  check[0]=s0;
  check[1]=s1;
  check[2]=s2;
  check[3]=s3;
  ihit[0] =ih0;
  ihit[1] =ih1;
  ihit[2] =ih2;
  ihit[3] =ih3;
  nPoints =4;
  for (int i=0; i<4; i++) {
    BPC[i]=i;
    BPCset[i]=true;    
  }
}
//-------------------------------------------------------
void AddPoint(int iBPC, int ih, double xx, double cc) {
  if (nPoints==4) {
    printf("[Track->AddPoint] more than 4 points not allowed, bye.\n");
    exit(-1);
  }
  if (iBPC<0 || iBPC>3) {
    printf("[Track->AddPoint] BPC#%d not allowed, bye.\n", iBPC);
    exit(-1);
  }
  ihit[iBPC]  =ih;
  x[iBPC]     =xx;
  check[iBPC] =cc;
  BPC[nPoints]=iBPC;
  BPCset[iBPC]=true;    
  nPoints++;
}
//-------------------------------------------------------
int GetBPChitIndex(int iBPC) {
  return ihit[iBPC];
}
//-------------------------------------------------------
int GetBPChitPosition(int iBPC) {
  return x[iBPC];
}
//-------------------------------------------------------
int GetBPChitCheckSum(int iBPC) {
  return check[iBPC];
}
//-------------------------------------------------------
int GetNpoints() {
  return nPoints;
}
//-------------------------------------------------------
int GetAxis() {
  return axis;
}
//-------------------------------------------------------
void Fit() {
  chi2=0.0;
  angle=0.0;
  x0=0.0;
  x0E=0.;
  angleE=0.;

  missingBPC=nBPC;

  if (nPoints==nBPC-1) { // only one missing BPC allowed for FIT
    for (int iBPC=0; iBPC<nBPC; iBPC++) {
      if (!BPCset[iBPC]) {
        missingBPC=iBPC;
//        break;
      }
//      printf("[Track->FIT] BPC#%d set=%d missing=%d\n", iBPC, BPCset[iBPC], missingBPC);
    }
  } else if (nPoints<nBPC-1) {
    printf("[Track->Fit()] --> %d of points not allowed, bye!\n", nPoints);
    exit(-1);
  }


  for (int iBPC=0; iBPC<4; iBPC++) {
    if (!BPCset[iBPC]) continue;
//    printf("[Track->FIT]\tadding point#%d z=%7.2f x=%6.2f\n", iBPC, BPC_Z[iBPC], x[iBPC]);
    angle+=w00[iBPC]*x[iBPC]*(BPC_Z[iBPC]-ss1[missingBPC]); 
    x0+=w00[iBPC]*x[iBPC];
//    printf("[Track->FIT]\t\t x0Sum=%f angleSum=%f\n", x0, angle);
  }
//  printf("[Track->FIT] Case#%d AverageZ=%.2fmm\t Base=%.2fmm\n",
//          missingBPC, ss1[missingBPC], sqrt(ssB[missingBPC]));

//  printf("[Track->FIT] after summing done:  x0Sum=%f angleSum=%f\n", x0, angle);
  angle/=ss0[missingBPC]*ssB[missingBPC];
  x0/=ss0[missingBPC];
  angleE=1.0/ssB[missingBPC];
  x0E=1.0/sqrt(ss0[missingBPC]); 

  for (int iBPC=0; iBPC<nBPC; iBPC++) {
    fit[iBPC]=angle*(BPC_Z[iBPC]-ss1[missingBPC])+x0;
    res[iBPC]=x[iBPC]-fit[iBPC];
    if (BPCset[iBPC]) {
      double diff=res[iBPC]/sigBPC[iBPC];
      chi2+=diff*diff;
    }
  }
  chi2/=(double)nPoints-2.;
  fitDone=true;
}
//-------------------------------------------------------
bool   CheckBPC(int i)    {return BPCset[i];}
int    GetMissingBPC()    {return missingBPC;}
double GetChi2()          {return chi2;}
double GetS4()            {return angle*(Z_S4-ss1[missingBPC])+x0;}
double GetCryo()          {return angle*(Z0-ss1[missingBPC])+x0;}
double GetResidual(int i) {return res[i];}
double GetFit(int i)      {return fit[i];}
double GetAtPlane(int i)  {return angle*(BPC_Z[i]-ss1[missingBPC])+x0;}
double GetAtZ(double z)   {return angle*(z-ss1[missingBPC])+x0;}
double GetAngle()         {return angle;}
double GetAngleError()    {return 1.0/sqrt(ssC[missingBPC]);}
double GetErrAtZ(double z){
  double    zz0=z-ss1[missingBPC];
  double result=1.0/ss0[missingBPC]+zz0*zz0/ssC[missingBPC];
  return sqrt(result);
} 
//-------------------------------------------------------
void Print() {
  char coord[1];
  if (axis==0) sprintf(coord, "X");
  else         sprintf(coord, "Y");

  if (fitDone) {
    printf("Track#%d Npoints=%d %s0=%5.2fmm angle=%6.3fmrad chi2=%f\n", 
            index, nPoints, coord, x0, 1000.0*angle, chi2);
  } else {
    printf("Track%s#%d %d points\n", coord, index, nPoints);
  }

  for (int iBPC=0; iBPC<nBPC; iBPC++) {
    if (!BPCset[iBPC]) continue;
    if (fitDone) {
      printf("\tBPC#%d %s=%7.2fmm fit=%7.2fmm residual=%7.3fmm | checkSum=%.0f\n",
              iBPC, coord, x[iBPC], fit[iBPC], res[iBPC], check[iBPC]);
    } else {
      printf("\tBPC#%d %s=%7.2fmm | checkSum=%.0f\n",
              iBPC, coord, x[iBPC], check[iBPC]);
    }
  }
}
};
